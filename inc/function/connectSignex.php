<?php

date_default_timezone_set("Asia/Bangkok");


function DbConnectSignex(){
  $db = "mysql:host=localhost;dbname=signnex";
  // $db = "mysql:host=onesittichok.co.th:3306;dbname=sso_queue_db";
  $user = "signnex";
  $pass = "signnex";


  $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
  try{
    return new PDO($db,$user,$pass, $options);
  }catch (Exception $e){
    return $e->getMessage();
  }
}



function DbQuerySignex($sql,$ParamData){
  try {
    $obj  = DbConnectSignex();
    //print_r($obj);
    $typeQuery = "1";
    if( strpos(strtolower($sql), "select") !== false ) {
      $obj->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING);
    } else {
      $obj->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_TO_STRING);
      $typeQuery = "2";
    }
    //echo $sql;
    $stm = $obj->prepare($sql);
    if($ParamData != null){
      for($x=1; $x<=count($ParamData); $x++)
      {
        $stm->bindParam($x,$ParamData[$x-1]);
      }
    }
    $id = "";
    $stm->execute();
    $arr = $stm->errorInfo();

    $strTrim = str_replace(' ', '', $sql);
    $strUpper = strtoupper($strTrim);
    $pos      = strpos($strUpper, "INSERTINTO");
    //echo $sql.">".$pos.", ";
    if($pos !== false)
    {
      //echo $sql.">".$pos.", ";
      try{
         $id = $obj->lastInsertId();
        //$result = $stm->fetch(PDO::FETCH_ASSOC);
        //echo ">>>".$id;
      }catch (PDOException $e)
      {
        echo $e;
        $obj->rollback();
      }
    }

    $num = 0;

    if($typeQuery == "1"){
			while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
	      $data['data'][] = $row;
				$num++;
	    }
		}

    $data['errorInfo'] = $arr;
    $data['dataCount'] = $num;
    $data['id'] = $id;

    if(isset($data)){
      if($num == 0){
        $data['data'] = "";
      }
      $data['status'] = 'success';
      return json_encode($data);
    }else{
      $data['status'] = 'Fail';
      $data['data'] = "";
      return json_encode($data);
    }
  } catch (Exception $e) {
    $data['dataCount'] = 0;
    $data['status'] = 'Fail';
    $data['data'] = "";
    $data['exception'] = $e;
    $data['errorInfo'][0] = $e->getCode();
    $data['errorInfo'][2] = $e->getMessage();
    return  json_encode($data);
    $e->getTraceAsString();
  }
}

?>
