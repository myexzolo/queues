<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบสนับสุนนการให้บริการ (Queue) - เข้าสู่ระบบ</title>
  <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>

  <?php include('../../inc/css-header.php'); session_destroy(); ?>
  <link rel="stylesheet" href="css/login.css">

</head>
<body class="hold-transition login-page" onload="showProcessbar();">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="login-box-body">
    <div align="center">
      <div class="login-logo">
        <img src="../../image/logo.png" style="height:150px;">
        <h2 style="color:#005c9e">ระบบสนับสุนนการให้บริการ (Queue)</h2>
      </div>
    </div>
    <p class="login-box-msg">ล็อกอินเข้าสู่ระบบ</p>

      <form id="formLogin" autocomplete="off" novalidate >
      <!-- <form method="post" action="ajax/AEDLogin.php" data-smk-icon="glyphicon-remove-sign" novalidate> -->
        <div id="show-form"></div>
      </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<?php include('../../inc/js-footer.php'); ?>
<script src="js/login.js"></script>
</body>
</html>
