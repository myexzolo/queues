<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$agency_code    = "";
$agency_name    = "";
$provice_code   = "";
$is_active      = "";
$agency_lat     = "";
$agency_lng     = "";
$ip_address     = "";
$agency_open    = "";
$agency_close   = "";


$disabled   = "";
$disabled2  = "disabled";
$bg         = "background-color:#fff;";
$display    = "";

if($action == 'ADD')
{
  $disabled2  = "";
}


if($action == 'EDIT' || $action == 'VIEW')
{
  if($action == 'VIEW')
  {
    $disabled = "disabled";
    $bg       = "";
    $display    = "display:none;";
  }

  $sql   = "SELECT * FROM t_agency WHERE agency_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;
  $agency_code    = $row[0]['agency_code'];
  $agency_name    = $row[0]['agency_name'];
  $provice_code   = $row[0]['provice_code'];
  $is_active      = $row[0]['is_active'];
  $agency_lat     = $row[0]['agency_lat'];
  $agency_lng     = $row[0]['agency_lng'];
  $agency_open    = $row[0]['agency_open'];
  $agency_close   = $row[0]['agency_close'];
  $ip_address     = $row[0]['ip_address'];

}

  $optionProvince = getoptionProvince($provice_code);
?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="agency_id" value="<?=$id?>">
<div class="modal-body">
        <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label>รหัสหน่วยงาน</label>
                <input type="text" <?= $disabled ?> OnKeyPress="return chkNumber(this)" value="<?= $agency_code ?>" name="agency_code" class="form-control"  placeholder="รหัสหน่วยงาน" required="">
              </div>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <label>ชื่อหน่วยงาน</label>
                <input type="text" <?= $disabled ?> value="<?= $agency_name ?>" name="agency_name" class="form-control" placeholder="ชื่อหน่วยงาน" required="">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>สถานะ</label>
                <select name="is_active" <?= $disabled ?> class="form-control " style="width: 100%;" required >
                  <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
                  <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>จังหวัด</label>
                <select name="provice_code" <?= $disabled ?> class="form-control select2" style="width: 100%;" required>
                  <option value="">จังหวัด</option>
                  <?= $optionProvince ?>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>Latitude</label>
                <input type="text" <?= $disabled ?> OnKeyPress="return chkNumber(this)" value="<?= $agency_lat ?>" name="agency_lat" class="form-control">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>Longitude</label>
                <input type="text" <?= $disabled ?> OnKeyPress="return chkNumber(this)" value="<?= $agency_lng ?>" name="agency_lng" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>IP Address</label>
                <input type="text" <?= $disabled ?> value="<?= $ip_address ?>" name="ip_address" class="form-control" required>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>เวลาเปิดให้บริการ</label>
                <input type="text" <?= $disabled ?> value="<?= $agency_open ?>" name="agency_open" class="form-control" required>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>เวลาปิดให้บริการ</label>
                <input type="text" <?= $disabled ?> value="<?= $agency_close ?>" name="agency_close" class="form-control" required>
              </div>
            </div>
        </div>
        <!-- /.row -->
    <!-- </div> -->
  <!-- </div> -->
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
  })
</script>
