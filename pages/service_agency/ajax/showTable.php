<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px">ลำดับ</th>
      <th style="width:50px">รหัสงาน</th>
      <th>ชื่องานบริการ (ภาษาไทย)</th>
      <th>ชื่องานบริการ (ภาษาอังกฤษ)</th>
      <th style="width:90px">เวลามาตราฐาน</th>
      <th >หมวดงาน</th>
      <th style="width:80px">จอง Online</th>
      <th style="width:80px">สถานะ</th>
      <th style="width:40px;">ดู</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:40px;">แก้ไข</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $agencyCode = $_SESSION['AGENCY_CODE'];

      $sql   = "SELECT sa.service_id as service_id_sa , s.*
                FROM t_service s LEFT JOIN t_service_agency sa
                ON s.service_id = sa.service_id and sa.agency_code = '$agencyCode'
                where s.is_active not in ('D')
                ORDER BY s.service_id";

      //echo $sql;

      $query      = DbQuery($sql,null);
      $json       = json_decode($query, true);
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      for($x = 0; $x < $dataCount; $x++)
      {
          $arrData = array();
          $service_id_sa = $rows[$x]['service_id_sa'];

          if(empty($service_id_sa))
          {
            //echo $rows[$x]['service_id'];
            $arrData['service_id']              = $rows[$x]['service_id'];
            $arrData['service_name_a']          = $rows[$x]['service_name'];
            $arrData['service_name_en_a']       = $rows[$x]['service_name_en'];
            $arrData['service_code_a']          = $rows[$x]['service_code'];
            $arrData['cat_service_id']          = $rows[$x]['cat_service_id'];
            $arrData['kpi_time_a']              = $rows[$x]['kpi_time'];
            $arrData['waiting_time_a']          = $rows[$x]['waiting_time'];
            $arrData['max_service_a']           = $rows[$x]['max_service'];
            $arrData['num_reserve_a']           = $rows[$x]['num_reserve'];
            $arrData['is_active']               = $rows[$x]['is_active'];
            $arrData['reserve_status_a']        = $rows[$x]['reserve_status'];
            $arrData['transfer_service_id_a']   = $rows[$x]['transfer_service_id'];
            $arrData['check_list_a']            = $rows[$x]['check_list'];
            $arrData['agency_code']             = $agencyCode;
            $arrData['status_send']             = 'N';

            $arrData  = chkDataEmpty($arrData);

            $arrData["date_create"] = date('Y/m/d H:i:s');
            $sql = DBInsertPOST($arrData,'t_service_agency');

            $query     = DbQuery($sql,null);
            $json      = json_decode($query, true);

            $service_agency_id  = $json['id'];

            $arrData["ref_code"] = $agencyCode."#C".$service_agency_id;//แก้ ID เปลี่ยน #C
            $arrData["action"]   = "ADD";

            $ipAgency    = getIPbyAgency($agencyCode);  //เปลี่ยน  ipAgency
            // $ipAgency    = $_SESSION['IP_CENTER'];  //เปลี่ยน  ipAgency

            $data_array  = array(
                               "functionName" => "manageServiceAgency",  ///แก้ ชื่อ Service
                               "dataJson" => $arrData,
                             );

            $url        = "http://$ipAgency/ws/service.php";


            $make_call = callAPI('POST', $url, json_encode($data_array));
            $response = json_decode($make_call, true);
            $status   = $response['status'];
            $data     = $response['data'];

            $arrUpdate['service_agency_id']     =  $service_agency_id; //แก้ ID
            $arrUpdate['ref_code']              =  $arrData["ref_code"];

            $sql = "";
            if($status == "200")
            {
                $arrUpdate['status_send']  =  "S";
            }else{
              $arrLog['url']          = $url;
              $arrLog['data']         = json_encode($data_array);
              $arrLog['table_name']   = 't_service_agency';// เปลี่ยน Table
              $arrLog['id_update']    = 'service_agency_id';// เปลี่ยน ID
              $arrLog['date_create']  = date('Y/m/d H:i:s');
              $arrLog['data_update']  = json_encode($arrUpdate);

              $sql = DBInsertPOST($arrLog,'t_log_send_service');
            }

            $sql .= DBUpdatePOST($arrUpdate,'t_service_agency','service_agency_id'); ///แก้ ชื่อ table
            DbQuery($sql,null);
          }
      }

      $sqls   = "SELECT *
                 FROM t_service_agency
                 where is_active not in ('D') and agency_code = '$agencyCode'
                 ORDER BY cat_service_id , service_code_a";

      //echo $sqls;
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      if($dataCount > 0)
      {
        foreach ($rows as $key => $value) {
          $is_active        = $value['is_active'];
          $reserve_status   = $value['reserve_status_a'];
          // $mem_username     = $value['mem_username']?;

          $activeTxt  = "";
          $bg         = "";
          $onclick    = "";
          $disabled   = "";

          $activeTxtR  = "";
          $bgR         = "";



          if($is_active == "Y")
          {
            $activeTxt = "ใช้งาน";
            $bg        = "bg-green-active";
          }else if($is_active == "N"){
            $activeTxt = "ไม่ใช้งาน";
            $bg        = "bg-gray";
          }

          if($reserve_status == "Y")
          {
            $activeTxtR = "เปิดจอง";
            $bgR        = "bg-green-active";
          }else if($reserve_status == "N"){
            $activeTxtR = "ไม่เปิดจอง";
            $bgR        = "bg-gray";
          }

    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="center"><?=$value['service_code_a']?></td>
      <td align="left"><?=$value['service_name_a']?></td>
      <td align="left"><?=$value['service_name_en_a']?></td>
      <td align="right"><?=$value['kpi_time_a']." นาที"?></td>
      <td align="left"><?=getCatServiceName($value['cat_service_id'])?></td>
      <td><div class="<?=$bgR?>"><?=$activeTxtR ?></div></td>
      <td><div class="<?=$bg?>"><?=$activeTxt ?></div></td>
      <td>
        <a class="btn_point text-green"><i class="fa fa-eye" onclick="showForm('VIEW','<?=$value['service_agency_id']?>')"></i></a>
      </td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['service_agency_id']?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php }} ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });
  })
</script>
