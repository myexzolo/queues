<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action                = isset($_POST['action'])?$_POST['action']:"";
$service_agency_id     = isset($_POST['service_agency_id'])?$_POST['service_agency_id']:"";

// $user_id  = $_SESSION['member']['']

//print_r($_POST);
unset($_POST["action"]);
$_POST["status_send"] = "N"; //เพิ่ม

if(empty($_POST["transfer_service_id_a"]))
{
  unset($_POST["transfer_service_id_a"]);
}

if($action == "ADD")
{
    unset($_POST["service_agency_id"]);//เปลี่ยน ID
    $_POST["date_create"] = date('Y/m/d H:i:s');
    $sql = DBInsertPOST($_POST,'t_service_agency');//เปลี่ยน table
}
else if($action == "EDIT")
{
    $sql = DBUpdatePOST($_POST,'t_service_agency','service_agency_id');//เปลี่ยน table, ID
}
else if($action == "DEL")
{
    $sql = "UPDATE t_service_agency SET is_active = 'D', status_send = 'N' WHERE service_agency_id = '$service_agency_id'";//เปลี่ยน table, ID

}

//echo $sql;

$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

// print_r($errorInfo);
// echo intval($row['errorInfo'][0]);

if(intval($errorInfo[0]) == 0){

    if($action == "ADD")
    {
      $service_agency_id  = $row['id']; ///แก้ ID
    }


    $sqls   = "SELECT * FROM t_service_agency WHERE service_agency_id = '$service_agency_id'"; //เปลี่ยน table

    $querys     = DbQuery($sqls,null);
    $json       = json_decode($querys, true);
    $rows       = $json['data'][0];

    $rows["action"]   = "UP";
    $agency_code = $rows["agency_code"];

    if($action == 'ADD')
    {
      $rows["ref_code"] = $agency_code."#C".$service_agency_id;   ///แก้ ID
      $rows["action"]   = "ADD";
    }
    $rows        = chkDataEmpty($rows);


    $ipAgency    = getIPbyAgency($agency_code);
    // $ipAgency    = $_SESSION['IP_CENTER'];  //เปลี่ยน  ipAgency

    $data_array  = array(
                       "functionName" => "manageServiceAgency",  ///แก้ ชื่อ Service
                       "dataJson" => $rows,
                     );

    $url        = "http://$ipAgency/ws/service.php";

    // echo $url;
    //echo json_encode($data_array);

    $make_call = callAPI('POST', $url, json_encode($data_array));
    $response = json_decode($make_call, true);
    $status   = $response['status'];
    $data     = $response['data'];

    $arrUpdate['service_agency_id']    =  $service_agency_id; //แก้ ID
    $arrUpdate['ref_code']             =  $rows["ref_code"];
    $arrUpdate['status_send']          =  "N";

    //print_r($response);
    $sql = "";

    if($status == "200")
    {
        $arrUpdate['status_send']  =  "S";
    }else{
      $arrLog['url']  = $url;
      $arrLog['data'] = json_encode($data_array,JSON_UNESCAPED_UNICODE);
      $arrLog['table_name']   = 't_service_agency';   // เปลี่ยน Table
      $arrLog['id_update']    = 'service_agency_id';// เปลี่ยน ID
      $arrLog['date_create']  = date('Y/m/d H:i:s');
      $arrLog['data_update']  = json_encode($arrUpdate);

      $sql = DBInsertPOST($arrLog,'t_log_send_service'); ///แก้ ชื่อ table
    }

    $sql .= DBUpdatePOST($arrUpdate,'t_service_agency','service_agency_id'); ///แก้ ชื่อ table
    DbQuery($sql,null);

    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}



?>
