<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$agencyCode = $_SESSION['AGENCY_CODE'];


$service_name_a     = "";
$service_name_en_a  = "";
$service_code_a     = "";
$cat_service_id     = "";
$kpi_time_a         = "";
$is_active          = "";
$check_list_a       = "";
$reserve_status_a       = "";
$transfer_service_id_a  = "";

$disabled   = "";
$disabled2  = "disabled";
$bg         = "background-color:#fff;";
$display    = "";

if($action == 'ADD')
{
  $disabled2  = "";
}


if($action == 'EDIT' || $action == 'VIEW')
{
  if($action == 'VIEW')
  {
    $disabled = "disabled";
    $bg       = "";
    $display    = "display:none;";
  }

  // $sql   = "SELECT s.*
  //           FROM t_service s WHERE service_id = '$id'";


  $sql   = "SELECT s.service_name,s.service_name_en,s.service_code,s.kpi_time,s.waiting_time,s.max_service,s.num_reserve,sa.*
            FROM t_service_agency sa, t_service s
            where sa.is_active not in ('D') and sa.service_id = s.service_id and  sa.service_agency_id = '$id'";


  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;
  $service_name           = $row[0]['service_name'];
  $service_name_en        = $row[0]['service_name_en'];
  $service_code           = $row[0]['service_code'];
  $kpi_time               = $row[0]['kpi_time'];
  $waiting_time           = $row[0]['waiting_time'];
  $max_service            = $row[0]['max_service'];
  $num_reserve            = $row[0]['num_reserve'];
  $service_name_a         = $row[0]['service_name_a'];
  $service_name_en_a      = $row[0]['service_name_en_a'];
  $service_code_a         = $row[0]['service_code_a'];
  $cat_service_id         = $row[0]['cat_service_id'];
  $kpi_time_a             = $row[0]['kpi_time_a'];
  $waiting_time_a         = $row[0]['waiting_time_a'];
  $max_service_a          = $row[0]['max_service_a'];
  $num_reserve_a          = $row[0]['num_reserve_a'];
  $is_active              = $row[0]['is_active'];
  $check_list_a           = $row[0]['check_list_a'];
  $reserve_status_a       = $row[0]['reserve_status_a'];
  $transfer_service_id_a  = $row[0]['transfer_service_id_a'];
}

  $optionCatService = getoptionCatService($cat_service_id);
?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="service_agency_id" value="<?=$id?>">
<div class="modal-body">
        <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label>รหัสงาน (ส่วนกลาง)</label>
                <input type="text" <?= $disabled ?> maxlength="2" value="<?= $service_code ?>" class="form-control" readonly>
              </div>
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <label>ชื่องานบริการ TH (ส่วนกลาง)</label>
                <input type="text" <?= $disabled ?> value="<?= $service_name ?>" class="form-control" placeholder="" readonly>
              </div>
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <label>ชื่องานบริการ EN (ส่วนกลาง)</label>
                <input type="text" <?= $disabled ?> value="<?= $service_name_en ?>" class="form-control" placeholder="" readonly>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>เวลามาตราฐาน</label>
                <input type="text" <?= $disabled ?> value="<?= $kpi_time ?>" class="form-control" readonly>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>เวลารอคอย</label>
                <input type="text" <?= $disabled ?> value="<?= $waiting_time ?>" class="form-control" readonly>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>จำนวนผู้รับบริการ</label>
                <input type="text" <?= $disabled ?> value="<?= $max_service ?>" class="form-control" readonly>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>จำนวนรับจองคิว</label>
                <input type="text" <?= $disabled ?> value="<?= $num_reserve ?>" class="form-control" readonly>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>หมวดงานบริการ (ส่วนกลาง)</label>
                <select <?= $disabled ?> class="form-control select2" style="width: 100%;" required disabled>
                  <option value="">หมวดงาน</option>
                  <?= $optionCatService ?>
                </select>
              </div>
            </div>
          </div>
            <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label>รหัสงาน</label>
                <input type="text" <?= $disabled ?> maxlength="2" value="<?= $service_code_a ?>" name="service_code_a" class="form-control"  required>
              </div>
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <label>ชื่องานบริการ TH</label>
                <input type="text" <?= $disabled ?> value="<?= $service_name_a ?>" name="service_name_a" class="form-control" placeholder="ชื่องานบริการ ภาษาไทย" required>
              </div>
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <label>ชื่องานบริการ EN</label>
                <input type="text" <?= $disabled ?> value="<?= $service_name_en_a ?>" name="service_name_en_a" class="form-control" placeholder="ชื่องานบริการ ภาษาอังกฤษ" >
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>เวลามาตราฐาน</label>
                <input type="text" <?= $disabled ?> OnKeyPress="return chkNumber(this)" value="<?= $kpi_time_a ?>" name="kpi_time_a" class="form-control" required>
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                <label>สถานะจอง Online</label>
                <select name="reserve_status_a" <?= $disabled ?> class="form-control " style="width: 100%;" required >
                  <option value="N" <?=@$reserve_status_a=='N'?"selected":""?>>ไม่เปิดจอง</option>
                  <option value="Y" <?=@$reserve_status_a=='Y'?"selected":""?>>เปิดจอง</option>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>จำนวนรับจองคิว</label>
                <input type="text" <?= $disabled ?> OnKeyPress="return chkNumber(this)" value="<?= @$num_reserve_a ?>" name="num_reserve_a" class="form-control">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>สถานะ</label>
                <select name="is_active" <?= $disabled ?> class="form-control " style="width: 100%;" required >
                  <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
                  <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>ส่งต่องานบริการ</label>
                <select name="transfer_service_id_a" <?= $disabled ?> class="form-control select2" style="width: 100%;" >
                  <option value="">&nbsp;</option>
                  <?php
                      $sql        = "SELECT * FROM t_cat_service WHERE is_active = 'Y' order by division_id";
                      $query      = DbQuery($sql,null);
                      $json       = json_decode($query, true);
                      $dataCount  = $json['dataCount'];
                      $row        = $json['data'];

                      for($x=0; $x < $dataCount; $x++)
                      {
                        $cat_service_id     = $row[$x]['cat_service_id'];
                        $cat_service_name   = $row[$x]['cat_service_name'];
                        echo "<optgroup label=\"$cat_service_name\">";

                        $con = "";
                        if($id != "")
                        {
                            $con = " and service_agency_id <> '$id' ";
                        }

                        $sql        = "SELECT * FROM t_service_agency
                                      WHERE is_active = 'Y' and cat_service_id = '$cat_service_id' and agency_code = '$agencyCode' $con order by service_name_a";
                        //echo $sql;
                        $query      = DbQuery($sql,null);
                        $json       = json_decode($query, true);
                        $dataCounts = $json['dataCount'];
                        $rows       = $json['data'];

                        for($i=0; $i < $dataCounts; $i++)
                        {
                          $service_id       = $rows[$i]['service_agency_id'];
                          $service_name     = $rows[$i]['service_name_a'];
                        ?>
                          <option value="<?=$service_id ?>" <?=@$service_id==$transfer_service_id_a?"selected":""?>><?=$service_name ?></option>
                        <?php
                        }
                      }
                  ?>
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>รายการเอกสารที่ต้องใช้ (Check list)</label>
                <div id="editor" <?= $disabled ?> ><?=@$check_list_a?></div>
                <input type="hidden" id="check_list" name="check_list_a">
              </div>
            </div>
        </div>
        <!-- /.row -->
    <!-- </div> -->
  <!-- </div> -->
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
    init();
  })
</script>
