<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<style>
.knob-label
{
  font-size: 20px !important;
}
.kiosk
{
  font-size: 80px;
  position: relative;
  top: 20px;
}

.closed
{
  color: #cccccc;
}

.error
{
  color: #ffffff !important;
}

.open
{
  cursor: pointer;
  color: #adadeb !important;
}
.open :hover
{
  color: #8484e1 !important;
}

.kos
{
  margin-bottom: 20px;
}
</style>

<?php
$agencyCode = $_POST['agency'];

$sqls   = "SELECT k.*, p.point_number
           FROM t_kiosk k,  t_point_service p
           where k.is_active not in ('D') and k.agency_code = '$agencyCode' and p.agency_code = '$agencyCode'
           and k.point_id = p.point_id
           ORDER BY point_id , kiosk_code";

//echo $sqls;
$querys     = DbQuery($sqls,null);
$json       = json_decode($querys, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$rows       = $json['data'];
?>

<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Kiosk
      <a onclick="showKiosk('<?=$agencyCode ?>')"><i class="fa fa-refresh" style="font-size:17px;"></i></a>
    </h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div>
  </div>
  <div class="box-body">
    <div class="row">
      <?php
      if($dataCount > 0)
      {
        foreach ($rows as $key => $value)
        {
          $kioskStartTime =  $value['kiosk_open']." - ".$value['kiosk_close']." น.";
          $status_kiosk   =  $value['status_kiosk'];
          $kiosk_code     =  $value['kiosk_code'];
          $kiosk_name     =  $value['kiosk_name'];
          $agency_code    =  $value['agency_code'];
          $mac            =  $value['mac'];
          $ip             =  $value['ip'];
          $refkiosk       =  $value['ref_code'];

          $active   = "";
          $disabled = "";
          $disabled2 = "disabled";
          $onclick  = "";
          $checked  = "";


          // if($ip != ""){
          //   error_reporting(0);
          //   $alive = fsockopen($ip, 80, $errno, $errstr, 2);
          //
          //   if(!$alive)
          //   {
          //     $status_kiosk == "C";
          //   }else{
          //     $status_kiosk == "O";
          //   }
          //  fclose($alive);
          // }




          $id = "id_".$kiosk_code;
          if($status_kiosk == "C")
          {
            $disabled = "disabled";
            $active   = "closed";
            $stateTxt = "(ปิดบริการ)";
          }else if($status_kiosk == "O")
          {
            $active   = "open";
            $checked  = "checked";
            $onclick = "onclick=\"showPerformanceInfo('$agency_code','$kiosk_code','$ip','$kiosk_name')\"";
            $stateTxt = "(เปิดบริการ)";
          }else if($status_kiosk == "E")
          {
            $active   = "error";
            $onclick = "onclick=\"showPerformanceInfo('$agency_code','$kiosk_code','$ip','$kiosk_name')\"";
            $stateTxt = "(ระบบมีปัญหา)";
          }

          if($mac != "" && $ip != ""){
            $disabled2 = "";
          }
      ?>
        <div class="col-md-6 text-center kos">
          <div class="relative">
            <button type="button" class="btn btn-block btn-flat  <?= $disabled ?>" style="height:200px" <?= $onclick ?>>
              <i class="icon ion-android-desktop kiosk <?= $active ?>"></i>
              <div class="knob-label"><b><?= $value['kiosk_name']." ".$stateTxt ?></b><br>เปิดเครื่อง <?= $kioskStartTime ?></div>
            </button>
            <div class="absolute">
              <label class="checkbox-inline" style="font-size: 18px;">
              สถานะ : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="<?= $id ?>" type="checkbox" <?=$checked?> class="status_kiosk" <?= $disabled2?>
              onchange="confirmActionKiosk(this, '<?= $kiosk_name ?>','<?= $ip ?>','<?= $mac?>', '<?=$id?>','<?= urlencode($refkiosk) ?>','<?=$agencyCode ?>')">
            </label>
          </div>
          </div>
        </div>
      <?php
        }
      }
      ?>

      <!-- <div class="col-md-6 text-center kos">
        <button type="button" class="btn btn-block btn-danger btn-flat disabled" style="height:200px">
          <i class="icon ion-android-desktop kiosk error"></i>
          <div class="knob-label error">Kiosk1 (ระบบมีปัญหา)<br>เปิดเครื่อง 07:00 - 16:30 น.</div>
        </button>
      </div>
      <div class="col-md-6 text-center kos">
        <button type="button" class="btn btn-block btn-flat" style="height:200px" ">
          <i class="icon ion-android-desktop kiosk open"></i>
          <div class="knob-label"><b>Kiosk2 (เปิดบริการ)</b><br>เปิดเครื่อง 07:00 - 16:30 น.</div>
        </button>
      </div> -->
      <!-- ./col -->
    </div>
  </div>
  <!-- /.box-body -->
</div>

<script>
  $(function() {
    $('.status_kiosk').bootstrapToggle({
      on: 'เปิด',
      off: 'ปิด'
    });
  })
</script>
