<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

require_once '../../../PHPGangsta/WakeOnLAN.php';

$ip  = $_POST['ip'];
$mac = $_POST['mac'];
// $ip  = '192.168.0.247';
// $mac = '48-BA-4E-3F-CD-B4';
$mac  = str_replace("-",":",$mac);

$status = \PHPGangsta\WakeOnLAN::wakeUp($mac, $ip);

//echo $status;
if($status)
{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => $status,'message' => 'success')));
}else
{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => $status,'message' => 'fail')));
}

?>
