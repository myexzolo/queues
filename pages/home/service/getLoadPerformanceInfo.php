<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

  $agencyCode = $_POST['agencyCode'];
  $kioskCode  = $_POST['kioskCode'];
  $kioskName  = $_POST['kioskName'];
  $ip         = $_POST['ip'];

  $arrData['agency_code'] = $agencyCode;
  $arrData['kiosk_code']  = $kioskCode;
  $arrData['ip']          = $ip;

  $data_array  = array(
                     "functionName" => "getPerformanceInfo",  ///แก้ ชื่อ Service
                     "dataJson" => $arrData,
                   );


  $url        = "http://$ip/queue_client/ws/service.php";



  $make_call = callAPI2('POST', $url, json_encode($data_array));
  $response = json_decode($make_call, true);
  $data     = json_decode($response['data'], true);
  $status   = $response['status'];

  //print_r($data);
  $percentOccupied = 0;
  $memTotal        = 0;

  $dataArr = array();

  if($status == "200")
  {

    $memory     = $data['Memory'];
    $Cpu        = $data['Cpu'];
    $DrivesInfo = $data['DrivesInfo'];
    $PrinterStatus  = $data['PrinterStatus'];


    $CpuTotal   = number_format($Cpu['Total']);
    //print_r($memory);
    $PhysicalAvailableMemoryInMiB    = $memory['PhysicalAvailableMemoryInMiB'];
    $TotalMemoryInMiB                = $memory['TotalMemoryInMiB'];
    // $percentFree                  = $memory['percentFree'];
    $percentOccupied                 = $memory['percentOccupied'];



    $memTotal = number_format($TotalMemoryInMiB/1024);
    //echo $PhysicalAvailableMemoryInMiB;
    $memColor = "#00a65a";
    $cpuColor = "#00a65a";
    if($percentOccupied > 80)
    {
      $memColor = "#dd4b39";
    }

    if($CpuTotal > 80)
    {
      $cpuColor = "#dd4b39";
    }

    $dataArr['CpuTotal']        = $CpuTotal;
    $dataArr['cpuColor']        = $cpuColor;
    $dataArr['percentOccupied'] = $percentOccupied;
    $dataArr['memColor']        = $memColor;

    if(isset($PrinterStatus))
    {
      $num =  count($PrinterStatus);
      //print_r($DrivesInfo);
      for($x = 0; $x < $num; $x++)
      {
        $Caption        = $PrinterStatus[$x]['Caption'];
        $PrinterState   = $PrinterStatus[$x]['PrinterState'];
        $Default        = $PrinterStatus[$x]['Default'];


        $idPrinter = "idPrinter_".$x;

        $statusPrinter = "";

        if($Default == "true"){
          $statusPrinter  = "Default";
        }else{
          $statusPrinter  = "";
        }

        if($statusPrinter != "")
        {
            $statusPrinter .= ", ".$PrinterState;
        }else
        {
            $statusPrinter = $PrinterState;
        }


        $dataArr['PrinterStatus'][$x]['Caption'] = $Caption;
        $dataArr['PrinterStatus'][$x]['statusPrinter'] = $statusPrinter;
        $dataArr['PrinterStatus'][$x]['PrinterState'] = $PrinterState;

      }
    }

    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'success' , 'data' => $dataArr)));
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'success' ,'data' => $dataArr)));

  }

  ?>
