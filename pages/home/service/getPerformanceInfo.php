<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<style>
.knob-label
{
  font-size: 20px !important;
}
.btn-info
{
  font-size: 20px !important;
  line-height: 20px;
  color: #ffffff;
}

.printonline
{
  font-size:30px;
  color:#2a2f5a;
}
.printoffline
{
  font-size:30px;
  color:#bbb;
}
</style>
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Kiosk</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div>
  </div>
  <?php
  $agencyCode = $_POST['agencyCode'];
  $kioskCode  = $_POST['kioskCode'];
  $kioskName  = $_POST['kioskName'];
  $ip         = $_POST['ip'];

  $arrData['agency_code'] = $agencyCode;
  $arrData['kiosk_code']  = $kioskCode;
  $arrData['ip']          = $ip;

  $data_array  = array(
                     "functionName" => "getPerformanceInfo",  ///แก้ ชื่อ Service
                     "dataJson" => $arrData,
                   );


  $url        = "http://$ip/queue_client/ws/service.php";
  //echo   $url;
  $make_call = callAPI2('POST', $url, json_encode($data_array));
  if(isset($make_call) && $make_call != "")
  {
    $response = json_decode($make_call, true);
    $resData  = isset($response['data'])?$response['data']:array();
    $data     = json_decode($response['data'], true);
    $status   = $response['status'];
  }



  $percentOccupied = 0;
  $memTotal        = 0;
  $CpuTotal        = 0;
  $DrivesInfo      = array();
  $memColor        = "#00a65a";
  $cpuColor        = "#00a65a";

  if(isset($data['status']) && $data['status'] == "500")
  {
    $status = "500";
  }

  if($status == "200")
  {
    $memory         = $data['Memory'];
    $Cpu            = $data['Cpu'];
    $DrivesInfo     = $data['DrivesInfo'];
    $PrinterStatus  = $data['PrinterStatus'];


    $CpuTotal   = number_format($Cpu['Total']);
    //print_r($memory);
    $PhysicalAvailableMemoryInMiB    = $memory['PhysicalAvailableMemoryInMiB'];
    $TotalMemoryInMiB                = $memory['TotalMemoryInMiB'];
    // $percentFree                  = $memory['percentFree'];
    $percentOccupied                 = $memory['percentOccupied'];

    $memTotal = number_format($TotalMemoryInMiB/1024);
    //echo $PhysicalAvailableMemoryInMiB;
    if($percentOccupied > 80)
    {
      $memColor = "#dd4b39";
    }

    if($CpuTotal > 80)
    {
      $cpuColor = "#dd4b39";
    }



  }

  ?>
  <div class="box-body">
    <div class="row">
      <div class="col-md-12 text-center">
        <label><b><?=$kioskName ?></b></label>
      </div>
      <div class="col-md-7 text-center">
        <div class="row">
            <div class="col-md-6 text-center">
              <input type="text" class="knob"  id="cpuTotal" value="<?=$CpuTotal?>" data-width="90" data-height="90" data-fgColor="<?=$cpuColor?>" readonly>
              <div class="knob-label">CPU</div>
            </div>
      <!-- ./col -->
            <div class="col-md-6 text-center">
              <input type="text" class="knob" id="percentOccupied" value="<?= @$percentOccupied ?>" data-width="90" data-height="90" data-fgColor="<?= @$memColor ?>" readonly>
              <div class="knob-label">Memory (<?= @$memTotal ?> GB)</div>
            </div>
        </div>
      </div>
      <div class="col-md-5 text-center">
        <div class="row">
          <?php
          if(isset($DrivesInfo))
          {
            $num =  count($DrivesInfo);
            //print_r($DrivesInfo);
            for($x = 0; $x < $num; $x++){
              $Name           = $DrivesInfo[$x]['Name'];
              $Size           = number_format(($DrivesInfo[$x]['Size']/1000000)*0.000931,2);
              $TotalFreeSpace = number_format(($DrivesInfo[$x]['TotalFreeSpace']/1000000)*0.000931,2);
              $strDisk = $TotalFreeSpace." GB free of ".$Size." GB";

              $p = number_format(($Size/($TotalFreeSpace + $Size))*100);

              $percent = $p."%";
          ?>
          <!-- <div class="col-md-6 text-center">
            <input type="text" class="knob" value="50" data-width="90" data-height="90" data-fgColor="#00c0ef">
            <div class="knob-label">Disk</div>
          </div> -->
          <div class="col-md-12 text-center">
            <table style="width:100%">
              <tr>
                <td style="width: 50px;"><i class="glyphicon glyphicon-hdd" style="font-size:30px; color:#2a2f5a"></i></td>
                <td>
                  <div class="progress-group">
                    <div class="progress-text-n"><?=$Name?></div>
                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="width: <?=  $percent ?>"></div>
                    </div>
                    <div class="progress-text2"><?=$strDisk?></div>
                  </div>
                </td>
              </tr>
            </table>
          </div>
        <?php }} ?>

        </div>
      </div>
      <div class="col-md-12">
        <hr>
      </div>
      <div class="col-md-12 text-center">
        <div class="row">
          <?php
          if(isset($PrinterStatus))
          {
            $num =  count($PrinterStatus);
            //print_r($DrivesInfo);
            for($x = 0; $x < $num; $x++)
            {
              $Caption        = $PrinterStatus[$x]['Caption'];
              $PrinterState   = $PrinterStatus[$x]['PrinterState'];
              $Default        = $PrinterStatus[$x]['Default'];


              $idPrinter = "idPrinter_".$x;
              $idPrinterName = "idPrinterName_".$x;
              $idPrinterCss = "idPrinterCss_".$x;

              $statusPrinter = "";

              if($Default == "true"){
                $statusPrinter  = "Default";
              }else{
                $statusPrinter  = "";
              }

              if($statusPrinter != "")
              {
                  $statusPrinter .= ", ".$PrinterState;
              }else
              {
                  $statusPrinter = $PrinterState;
              }

              $printcss = "printonline";
              if($PrinterState == "Offline")
              {
                $printcss = "printoffline";
              }

          ?>

          <div class="col-md-6 text-center" style="margin-bottom:10px;">

             <table style="width:100%">
               <tr>
                 <td style="width:50px;" align="left"><i class="glyphicon glyphicon-print <?=$printcss ?>" id="<?= $idPrinterCss ?>"></i></td>
                 <td align="left">
                   <div style="font-size:18px;" id="<?= $idPrinterName ?>"><?= $Caption ?></div>
                   <div style="font-size:16px;color:#aaa" id="<?= $idPrinter ?>"><?= $statusPrinter ?></div>
                 </td>
              </tr>
             </table>
          </div>
        <?php }} ?>

        </div>
      </div>

      <div class="col-md-12">
        <button type="button" class="btn btn-block btn-info btn-flat" onclick="showKiosk(<?=$agencyCode;?>);">
          กลับ
        </button>
      </div>
      <!-- ./col -->
    </div>
  </div>
  <!-- /.box-body -->
</div>
<script src="../../plugins/jquery-knob/jquery.knob.min.js"></script>
<script>
  $(function () {
    $(".knob").knob();
  })

</script>
