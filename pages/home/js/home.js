$(function () {
  // $('#dateRageShow').hide();
  // $('.connectedSortable').sortable({
  //   containment         : $('section.content'),
  //   placeholder         : 'sort-highlight',
  //   connectWith         : '.connectedSortable',
  //   handle              : '.box-header, .nav-tabs',
  //   forcePlaceholderSize: true,
  //   zIndex              : 999999
  // });
  // $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');
  //
  // // jQuery UI sortable for the todo list
  // $('.todo-list').sortable({
  //   placeholder         : 'sort-highlight',
  //   handle              : '.handle',
  //   forcePlaceholderSize: true,
  //   zIndex              : 999999
  // });
  //

  showChart(1);

  var role  = $('#role').val();
  if(role == 'admin')
  {
    // checkApprove();
    showChart(1);
    setInterval(function(){
      // checkApprove();
      showChart(1);
    }, 5000);
  }

  $('#daterage').daterangepicker(
    {
      locale: {
        format: 'DD/MM/YYYY',
        daysOfWeek: [
           "อา",
           "จ",
           "อ",
           "พ",
           "พฤ",
           "ศ",
           "ส"
       ],
       monthNames: [
           "มกราคม",
           "กุมภาพันธ์",
           "มีนาคม",
           "เมษายน",
           "พฤษภาคม",
           "มิถุนายน",
           "กรกฎาคม",
           "สิงหาคม",
           "กันยายน",
           "ตุลาคม",
           "พฤศจิกายน",
           "ธันวาคม"
       ]
      }
    }
  );

  // searchProject();
});


// var gdpData = {
//   "TH-57": 200,
//   "TH-56": 100,
//   "TH-55": 50,
//   "TH-54": 200
// };
function searchQueue(d)
{
   if(d == "1")
   {
     $("#btn1_bg").removeClass("bg-teal");
     $("#btn15_bg").removeClass("bg-navy");
     $("#btn30_bg").removeClass("bg-navy");

     $("#btn1_bg").addClass("bg-navy");
     $("#btn15_bg").addClass("bg-teal");
     $("#btn30_bg").addClass("bg-teal");
   }
   else if(d == "15")
   {
     $("#btn1_bg").removeClass("bg-navy");
     $("#btn15_bg").removeClass("bg-teal");
     $("#btn30_bg").removeClass("bg-navy");

     $("#btn1_bg").addClass("bg-teal");
     $("#btn15_bg").addClass("bg-navy");
     $("#btn30_bg").addClass("bg-teal");
   }
   else if(d == "30")
   {
     $("#btn1_bg").removeClass("bg-navy");
     $("#btn15_bg").removeClass("bg-navy");
     $("#btn30_bg").removeClass("bg-teal");

     $("#btn1_bg").addClass("bg-teal");
     $("#btn15_bg").addClass("bg-teal");
     $("#btn30_bg").addClass("bg-navy");
   }
}

function checkApprove()
{
  $( document ).ajaxStart(function() {
    $(".loadingImg").addClass('none');
  });
  $.get("ajax/checkApprove.php")
     .done(function( data ) {
       //console.log(data);
       $("#checkApprove").html(data.count);
   });
}

function searchProject(){
  var daterage    = $('#daterage').val();
  var userLogin  = $('#user_login').val();
  var role  = $('#role').val();
  var res = daterage.split("-");
  var dateStart = dateThToEn(res[0].trim(),"dd/mm/yyyy","/");
  var dateEnd = dateThToEn(res[1].trim(),"dd/mm/yyyy","/");


  $.post("ajax/showProjectList.php",{dateStart:dateStart,dateEnd:dateEnd,userLogin:userLogin,role:role})
    .done(function( data ) {
      $('#show-form').html(data);
  });
}

function confirmActionKiosk(obj,kioskName,ip,mac,id,refkiosk,agency)
{
  refkiosk = encodeURIComponent(refkiosk);
  var str = "ปิด";
  if(obj.checked)
  {
    str = "เปิด";
  }
  $.smkConfirm({
    text:'ยืนยันการ'+ str + 'เครื่อง ' + kioskName +' ?',
    accept:'ยืนยัน',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      if(obj.checked)
      {
        $.post("service/wol.php",{ip:ip,mac:mac,op:"wol",refkiosk:refkiosk})
          .done(function( data ){
            //console.log(data);
            if(!data.error)
            {
              $.smkAlert({text: 'success', type:'success'});
            }else{
              $.smkAlert({text: data.data, type:'danger'});
            }
            setTimeout(function() {showKiosk(agency);}, 60000);
        });
      }else{
        $.post("service/settimeCloseKiosk.php",{ip:ip})
          .done(function( data ) {
            if(data.status == "success")
            {
              $.smkAlert({text: 'success', type:'success'});
            }else{
              //$.smkAlert({text: 'fail', type:'danger'});
              $.smkAlert({text: 'success', type:'success'});
            }
            setTimeout(function(){showKiosk(agency);}, 30000);
        });


      }
    }else{
      if(obj.checked)
      {
        obj.checked = false;
        $('#'+ id).bootstrapToggle('off');
      }else{
        obj.checked = true;
        $('#'+ id).bootstrapToggle('on');
      }
    }
  });
}

function getMap()
{
  // $.post("ajax/class.php",{week:week})
  //   .done(function( data ) {
  //     $("#showClass").html( data );
  // });
  //ปกติ #00b16a
  //กลาง #00b16a
  //เยอะ #d91e18
  $('#map').vectorMap({
    map: 'th_mill',
    backgroundColor: 'transparent',
    regionStyle: {
                    initial: {
                      fill: '#8d8d8d'
                    }
                  },
    series: {
      regions: [{
        values: gdpData,
        scale: {"200": "#d91e18","100": "#f5e51b","50": "#00b16a"},
        normalizeFunction: 'polynomial'
      }]
    },
    onRegionTipShow: function(e, el, code){
      el.html(el.html()+' (จำนวน '+gdpData[code]+' ราย)');
    }

  });
}


function showChart(day)
{
  $.post("service/chart.php",{day:day})
    .done(function( data ) {
      $("#show-data").html( data );
      initBarChart();
  });
}

var interval;
function showPerformanceInfo(agencyCode,kioskCode,ip,kioskName)
{
  clearInterval(interval);
  $.post("service/getPerformanceInfo.php",{agencyCode:agencyCode,kioskCode:kioskCode,ip:ip,kioskName:kioskName})
    .done(function( data ) {
      $("#show-performance").html( data );
      interval = setInterval(function(){loadPerformanceInfo(agencyCode,kioskCode,ip,kioskName); },3000);
  });
}

function loadPerformanceInfo(agencyCode,kioskCode,ip,kioskName)
{
  $.post("service/getLoadPerformanceInfo.php",{agencyCode:agencyCode,kioskCode:kioskCode,ip:ip,kioskName:kioskName})
    .done(function( data ) {
      var dataPf = data.data;
      $("#cpuTotal").val(dataPf.CpuTotal);
      $("#cpuTotal").attr('data-fgColor', dataPf.cpuColor);
      $("#percentOccupied").val(dataPf.percentOccupied);
      $("#percentOccupied").attr('data-fgColor', dataPf.memColor);

      $(".knob").trigger('change');

      var printerStatusArr = dataPf.PrinterStatus;
      for(var x=0; x < printerStatusArr.length; x++)
      {
        var idPrinter     =   "idPrinter_" + x;
        var idPrinterName =   "idPrinterName_" + x;
        var idPrinterCss  =   "idPrinterCss_" + x;

        var caption       =  printerStatusArr[x].Caption;
        var statusPrinter =  printerStatusArr[x].statusPrinter;
        var printerState =  printerStatusArr[x].PrinterState;

          $("#"+idPrinter).html(statusPrinter);
          $("#"+idPrinterName).html(caption);

          if(printerState == "Offline")
          {
              $("#"+idPrinter).addClass('printoffline');
              $("#"+idPrinter).removeClass('printonline');
          }else{
              $("#"+idPrinter).addClass('printonline');
              $("#"+idPrinter).removeClass('printoffline');
          }
      }
      // $("#show-performance").html( data );
  });
}

function showKiosk(agency)
{
  // alert("xxx");
  clearInterval(interval);
  $( document ).ajaxStart(function() {
    $(".loadingImg").addClass('none');
  });
  $.post("service/getKiosk.php",{agency:agency})
    .done(function( data ) {
      $("#show-performance").html( data );
  });
}

function showClass(week)
{
  $.post("ajax/class.php",{week:week})
    .done(function( data ) {
      $("#showClass").html( data );
  });
}

function showClass1(week)
{
  $.post("ajax/class1.php",{week:week})
    .done(function( data ) {
      $("#showClass").html( data );
  });
}


function showClass2(week)
{
  $.post("ajax/class2.php",{week:week})
    .done(function( data ) {
      $("#showClass").html( data );
  });
}
