<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";


$cat_service_name = "";
$division_id      = "";
$is_active        = "";

$disabled   = "";
$disabled2  = "disabled";
$bg         = "background-color:#fff;";
$display    = "";

if($action == 'ADD')
{
  $disabled2  = "";
}


if($action == 'EDIT' || $action == 'VIEW')
{
  if($action == 'VIEW')
  {
    $disabled = "disabled";
    $bg       = "";
    $display    = "display:none;";
  }

  $sql   = "SELECT * FROM t_cat_service WHERE cat_service_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;
  $cat_service_name     = $row[0]['cat_service_name'];
  $division_id          = $row[0]['division_id'];
  $is_active            = $row[0]['is_active'];
}

  $optionDivision = getoptionDivision($division_id);
?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="cat_service_id" value="<?=$id?>">
<div class="modal-body">
        <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>ชื่อหมวดงานบริการ</label>
                <input type="text" <?= $disabled ?> value="<?= $cat_service_name ?>" name="cat_service_name" class="form-control" placeholder="ชื่อหมวดงานบริการ" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>ส่วนงาน</label>
                <select name="division_id" <?= $disabled ?> class="form-control select2" style="width: 100%;" required>
                  <option value="">ส่วนงาน</option>
                  <?= $optionDivision ?>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>สถานะ</label>
                <select name="is_active" <?= $disabled ?> class="form-control " style="width: 100%;" required >
                  <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
                  <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
                </select>
              </div>
            </div>
        </div>
        <!-- /.row -->
    <!-- </div> -->
  <!-- </div> -->
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
  })
</script>
