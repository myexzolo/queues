$(function() {
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        var s_year =  parseInt(start.format('YYYY')) + 543;
        var e_year =  parseInt(end.format('YYYY')) + 543;
        var startDate   = start.format('DD/MM') + "/" + s_year;
        var endDate     = end.format('DD/MM') + "/" + e_year;

        $('#startDate').val(start.format('YYYY-MM-DD'));
        $('#endDate').val(end.format('YYYY-MM-DD'));
        $('#reportrange span').html(startDate + ' - ' + endDate);
    }
    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'วันนี้': [moment(), moment()],
           'เมื่อวาน': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           '7 วันก่อน': [moment().subtract(6, 'days'), moment()],
           '15 วันก่อน': [moment().subtract(15, 'days'), moment()],
           '30 วันก่อน': [moment().subtract(29, 'days'), moment()],
           'เดือนนี้': [moment().startOf('month'), moment().endOf('month')],
           'เดือนก่อน': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'ปีนี้': [moment().startOf('year'), moment().endOf('year')],
           'ปีก่อน': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
        },
        locale: {
            "applyLabel": "เลือก",
            "cancelLabel": "ยกเลิก",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "กำหนดช่วงเวลา",
            "daysOfWeek": [
                "อา",
                "จ",
                "อ",
                "พ",
                "พฤ",
                "ศ",
                "ส"
            ],
            "monthNames": [
                "มกราคม",
                "กุมภาพันธ์",
                "มีนาคม",
                "เมษายน",
                "พฤษภาคม",
                "มิถุนายน",
                "กรกฎาคม",
                "สิงหาคม",
                "กันยายน",
                "ตุลาคม",
                "พฤศจิกายน",
                "ธันวาคม"
            ],
            "firstDay": 1
        }
    }, cb);
    cb(start, end);

    showTable();
});

function showTable()
{
  var startDate   = $('#startDate').val();
  var endDate     = $('#endDate').val();
  var agencyStr  = $('#agency_str').val();
  var serviceStr  = $('#serviceStr').val();
  //console.log(startDate);
  $.post( "ajax/showTable.php",{startDate:startDate,endDate:endDate,agencyStr:agencyStr,serviceStr:serviceStr})
  .done(function( data ) {
    $("#showTable").html( data );
  });
}

function printPdf()
{
  var startDate  = $('#startDate').val();
  var endDate    = $('#endDate').val();
  var dataCount  = $('#dataCount').val();
  var agencyStr  = $('#agency_str').val();
  var serviceStr  = $('#serviceStr').val();

  if(dataCount > 0)
  {
    var pram = "?startDate="+ startDate + "&endDate=" + endDate + "&agencyStr=" + agencyStr+ "&serviceStr=" + serviceStr;
    var url = 'print.php'+ pram;
    postURL_blank(url);
  }else{
    $.smkAlert({text: "ไม่พบข้อมูล",type: "warning"});
  }
}
