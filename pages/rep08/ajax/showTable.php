<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<style>
th{
  vertical-align: middle !important;
}
</style>
<?php
$startDate  = $_POST['startDate'];
$endDate    = $_POST['endDate'];
$agencyStr  = $_POST['agencyStr'];
$serviceStr = $_POST['serviceStr'];

$startDateStr = DatetoThaiMonth($startDate);
$endDateStr   = DatetoThaiMonth($endDate);

$textDate = "";
if($startDate == $endDate){
  $textDate = "วันที่ ".$startDateStr;
}else{
  $textDate = "ตั้งแต่วันที่ ".$startDateStr." - ".$endDateStr;
}

$startDate  = $_POST['startDate']." 00:00:01";
$endDate    = $_POST['endDate']." 23:59:59";

$con = "";
if($agencyStr != ""){
  $con = " and q.agency_code = '$agencyStr'";
}

$str = "ทุกประเภทบริการ";

if($serviceStr != ""){
  $arrService = explode(":", $serviceStr);
  $str  = $arrService[1];
  $con .= " and q.service_id = '$arrService[0]'";
}

$sql = "SELECT q.*,
        TIMESTAMPDIFF(SECOND,q.date_start,q.date_service) as wait_service,
        TIMESTAMPDIFF(SECOND,q.date_service,q.date_end) as time_service,
        TIMESTAMPDIFF(SECOND,q.date_start,q.date_end) as time_trans,
        a.agency_name, s.kpi_time_a, s.service_name_a
        FROM t_trans_queue q, t_agency a, t_service_agency s
        WHERE q.date_start between '$startDate' and '$endDate' and status_queue = 'E'
        and  q.agency_code = a.agency_code and q.agency_code = s.agency_code and q.service_id = s.service_id $con
        ORDER BY q.agency_code,q.service_id,q.date_start";

$querys     = DbQuery($sql,null);
$json       = json_decode($querys, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$rows       = $json['data'];

?>
<div class="box-body">
<div align="center">
  <p style="font-size:24px;font-weight:600">รายงานสรุปการรับบริการของแต่ละประเภทให้บริการ</p>
  <p style="font-size:24px;font-weight:600"><?= $str ?></p>
  <p style="font-size:22px;font-weight:600"><?=$textDate?></p>
</div>
<input type="hidden" id="dataCount" value="<?=$dataCount?>">
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px;">ลำดับที่</th>
      <th >รหัสคิว</th>
      <th >งานบริการ</th>
      <th style="width:200px;">เวลารอของผู้รับบริการ</th>
      <th style="width:200px;">เวลาที่ใช้ในการให้บริการ</th>
      <th style="width:200px;">เวลาเก็บคิว</th>
      <th style="width:200px;">เวลาที่ให้บริการทั้งหมด</th>
    </tr>
  </thead>
<tbody>
  <?php
      $numShow = 0;
      $serviceIdTmp = "";
      for($i = 0; $i < $dataCount; $i++)
      {
          $kpi_time_a   = $rows[$i]['kpi_time_a'];
          $agency_code  = $rows[$i]['agency_code'];
          $wait_service = $rows[$i]['wait_service'];
          $time_service = $rows[$i]['time_service'];
          $time_holes   = $rows[$i]['time_holes'];
          $time_trans   = $rows[$i]['time_trans'];
          $service_id   = $rows[$i]['service_id'];

          $time_trans   -= $time_holes;
          $wait_service = gmdate("H:i:s", $wait_service);
          $time_service = gmdate("H:i:s", $time_service);
          $time_holes   = gmdate("H:i:s", $time_holes);
          $time_trans   = gmdate("H:i:s", $time_trans);

          if($serviceIdTmp != $service_id)
          {
            $numShow = 0;
            $serviceIdTmp = $service_id;
          }

          $agency_name   = $rows[$i]['agency_name'];
          $numShow++;

      ?>
        <tr class="text-center">
          <td ><?=$numShow;?></td>
          <td style="text-align:center;"><?= $rows[$i]['queue_code'] ?></td>
          <td style="text-align:left;"><?= $rows[$i]['service_name_a'] ?></td>
          <td style="text-align:center;"><?= $wait_service ?></td>
          <td style="text-align:center;"><?= $time_service ?></td>
          <td style="text-align:center;"><?= $time_holes ?></td>
          <td style="text-align:center;"><?= $time_trans ?></td>
        </tr>
      <?php
      }
    ?>
  </tbody>
</table>
</div>
<div class="box-footer">
  <button type="button" class="btn btn-success btn-flat pull-right btn-rep" onclick="printPdf()" >พิมพ์</button>
</div>
<script>
  $(function () {
    var groupColumn = 2;

    $('#tableDisplay').DataTable({
      'paging'        : true,
      'lengthChange'  : false,
      'searching'     : false,
      'ordering'      : false,
      'info'          : true,
      'autoWidth'     : false,
      'columnDefs'    : [{ "visible": false, "targets": groupColumn }],
      'order'         : [[ groupColumn, 'asc' ]],
      'drawCallback'  : function ( settings ) {
                        var api = this.api();
                        var rows = api.rows( {page:'current'} ).nodes();
                        var last=null;

                        api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                          if ( last !== group )
                          {
                            $(rows).eq( i ).before(
                                '<tr class="group"><td colspan="6"><b>'+group+'</b></td></tr>'
                            );
                            last = group;
                          }
                      } );
                      }
    })

  })
</script>
