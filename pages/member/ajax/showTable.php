<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px">ลำดับ</th>
      <th>ชื่่อ - นามสกุล</th>
      <th>เลขที่บัตรประชาชน</th>
      <th>วันเดือนปีเกิด</th>
      <th>เบอร์โทร</th>
      <th>Email</th>
      <th>UserName</th>
      <th style="width:90px">สถานะ</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:100px;">Re Password</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT *
                 FROM t_member
                 where is_active not in ('D')
                 ORDER BY date_create DESC";

      //echo $sqls;
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      if($dataCount > 0)
      {
        foreach ($rows as $key => $value) {

          $hide_list = trim($value['hide_list']);
          $mem_dob   = $value['mem_dob'];
          $value = checkConsent($value,$hide_list,'F'); // F = ทั้งหมด , H = ครึ่ง , ตัวเลข


          $is_active        = $value['is_active'];
          $memname          = $value['mem_name']." ".$value['mem_last'];

          $activeTxt  = "";
          $bg         = "";
          $onclick    = "";
          $disabled   = "";



          if($is_active == "Y")
          {
            $activeTxt = "ใช้งาน";
            $bg        = "bg-green-active";
          }else if($is_active == "N"){
            $activeTxt = "ไม่ใช้งาน";
            $bg        = "bg-gray";
          }


          if($hide_list == ""){
            $mem_dob = DateThai($mem_dob);
          }else{
            $pos =  strpos($value['mem_dob'],"X");
            if($pos >= 0)
            {
              $mem_dob = $value['mem_dob'];
            }else
            {
              $mem_dob = DateThai($mem_dob);
            }

          }
    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="left"><?=$memname ?></td>
      <td align="center"><?=$value['mem_idcard'] ?></td>
      <td align="center"><?= $mem_dob ?></td>
      <td align="center"><?= $value['tel_no'] ?></td>
      <td align="left"><?= $value['email'] ?></td>
      <td align="left"><?= $value['username'] ?></td>
      <td><div class="<?=$bg?>"><?=$activeTxt ?></div></td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point text-yellow"><i class="fa fa-key" onclick="resetPass('<?=$value['mem_id']?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php }} ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });
  })
</script>
