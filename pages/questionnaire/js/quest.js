
openModalQuest();

function openModalQuest()
{
  var tr = $('#tr').val()
  $.post("ajax/form.php",{ref_code:tr,status:'R'})
    .done(function( data ) {
      $('#show-form').html(data);
      $('#myModal').modal({backdrop:'static'});

  });
}



$('#formAED').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAED').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
        $('#formAED').smkClear();
        openModalQuest();
    }).fail(function (jqXHR, textStatus) {
        console.log(jqXHR);
    });
  }
});
