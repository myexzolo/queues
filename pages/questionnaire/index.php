<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบสนับสุนนการให้บริการ (Queue) - แสดงความคิดเห็น</title>
  <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>

  <?php include('../../inc/css-header.php'); session_destroy(); ?>
  <link rel="stylesheet" href="css/quest.css">
  <?php
      $tr = @$_GET['ref'];
      //echo $tr;
  ?>
</head>
<body class="hold-transition login-page" onload="showProcessbar();">

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-md" role="document" >

    <div class="modal-content">
      <div class="modal-header">
        <div align="center">
          <div class="login-logo">
            <img src="../../image/logo.png" style="height:120px;">
            <h2 style="color:#005c9e">ระบบสนับสุนนการให้บริการ (Queue)</h2>
          </div>
        </div>
        <p class="login-box-msg">แสดงความคิดเห็น</p>
      </div>
      <input type="hidden" id="tr" value="<?= $tr?>">
      <form id="formAED" novalidate enctype="multipart/form-data">
      <!-- <form novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
          <div id="show-form"></div>
      </form>
    </div>
  </div>
</div>
<!-- /.login-box -->

<?php include('../../inc/js-footer.php'); ?>
<script src="js/quest.js"></script>
</body>
</html>
