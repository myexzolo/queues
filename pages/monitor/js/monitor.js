$(function () {
  pushMenu();
  showTable();
  setInterval(function(){checkStatusDevice()},3000);
})


function pushMenu()
{
  if($( window ).width() < 1900){
     $('[data-toggle="push-menu"]').pushMenu('toggle');
  }
}


function showTable(){
  var agencyCode = $("#agencyCode").val();
  $.post("ajax/showTable.php",{agencyCode:agencyCode})
    .done(function( data ) {
      $('#showTable').html(data);
  });
}


function checkStatusDevice(){
  $( document ).ajaxStart(function() {
    $(".loadingImg").addClass('none');
  });
  var agencyCode = $("#agencyCode").val();
  $.post("ajax/checkStatusDevice.php",{agencyCode:agencyCode})
    .done(function( data ) {
      var dataArr =  data.data;
      var num = dataArr.length;
      for(var x=0; x < num ; x++)
      {
        var deviceArr = dataArr[x];
        // console.log(deviceArr);
        var id        = deviceArr.id;
        var activeTxt = deviceArr.activeTxt;
        var bg        = deviceArr.bg;

        $('#'+id).html(activeTxt);
        if(bg == "bg-red")
        {
          $('#'+id).removeClass("bg-yellow");
          $('#'+id).removeClass("bg-green");
        }
        if(bg == "bg-yellow")
        {
          $('#'+id).removeClass("bg-red");
          $('#'+id).removeClass("bg-green");;
        }
        if(bg == "bg-green")
        {
          $('#'+id).removeClass("bg-yellow");
          $('#'+id).removeClass("bg-red");
        }
        $('#'+id).addClass(bg);
      }
  });
}

function monitor(monitor_id,ip_source)
{
  $.ajax({
      url: "ajax/vnc.php",
      method: "POST",
      data: { id : monitor_id },
      error: function(xhr, textStatus, errorThrown){
        if(textStatus === 'timeout')
        {
            monitor(monitor_id,ip_source);
        }
      },
      success: function(data){
          //do something
          console.log(data);
          var strArr = ip_source.split(":");
          ip_host = strArr[0];
          port    = strArr[1];

          var url = "../../novnc/?ip_host="+ ip_host +"&port="+port;
          console.log(url);
          postURL_blank(url);
      },
      timeout: 2000
  });
}

function showForm(action,id){
  $.post("ajax/form.php",{action:action,id:id})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}



function del(id,name){
  $.smkConfirm({
    text:'ยืนยันการลบข้อมูล  : '+ name +' ?',
    accept:'Yes',
    cancel:'No'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/AED.php",{action:'DEL',monitor_id:id})
        .done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#fff',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

function checknumber(inputs){
  var valid = /^\d{0,10}(\.\d{0,2})?$/.test(inputs.value),
      val = inputs.value;
  if(!valid){
      inputs.value = val.substring(0, val.length - 1);
  }
}

function chkNumber(ele)
{
    var vchar = String.fromCharCode(event.keyCode);
    if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
    ele.onKeyPress=vchar;
}


$('#formAED').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAED').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAED').smkClear();
        showTable();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});
