<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/connectSignex.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);



$agencyCode = $_POST['agencyCode'];

$con = "";

$data = array();

if($agencyCode != "")
{
  $con = " and r.agency_code = '$agencyCode'";
}


$sqls   = "SELECT r.*, a.agency_name
           FROM t_monitor r, t_agency a
           where r.is_active = 'Y' and r.agency_code = a.agency_code $con
           ORDER BY r.agency_code";

//echo $sqls;
$querys     = DbQuery($sqls,null);
$json       = json_decode($querys, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$rows       = $json['data'];

if($dataCount > 0)
{
  $num = 0;
  foreach ($rows as $key => $value)
  {
      $monitor_name     = $value['monitor_name'];
      $activeTxt = "offline";
      $bg        = "bg-red";

      $onclick    = "";
      $disabled   = "";

      $sqlp       = "SELECT * FROM player where name = '$monitor_name'";

      $queryp     = DbQuerySignex($sqlp,null);
      $jsonp      = json_decode($queryp, true);
      $dataCountp = $jsonp['dataCount'];
      $rowp       = $jsonp['data'];

      $idDivStatus  = "";
      if($dataCountp > 0)
      {
        $last_active  = $rowp[0]['last_active'];
        $id           = $rowp[0]['id'];

        $idDivStatus  = "div_".$id;
        $dateNow      = date("Y-m-d H:i:s", strtotime('-7 hours'));

        $to_time    = strtotime($last_active);
        $from_time  = strtotime($dateNow);

        $diff =  round(abs($to_time - $from_time) / 60,2). " minute";

        // echo   $last_active." >> ".$dateNow." : ". $diff;
        if( $diff > 1.2)
        {
          $activeTxt = "offline";
          $bg        = "bg-red";
        }else{
          $activeTxt = "online";
          $bg        = "bg-green";
        }

        $data[$num]['id']         = $idDivStatus;
        $data[$num]['activeTxt']  = $activeTxt;
        $data[$num]['bg']         = $bg;

        $num++;
      }
    }
  }

  header('Content-Type: application/json');
  exit(json_encode(array('data' => $data)));
?>
