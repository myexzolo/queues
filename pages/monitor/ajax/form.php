<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";



$agency_code        = "";
$monitor_name	      = "";
$ip_source          = "";
$ip_target          = "";
$monitor_desc       = "";


$disabled   = "";
$disabled2  = "disabled";
$bg         = "background-color:#fff;";
$display    = "";

$lde = "";

if($action == 'ADD')
{
  $disabled2  = "";
}


if($action == 'EDIT' || $action == 'VIEW')
{
  if($action == 'VIEW')
  {
    $disabled = "disabled";
    $bg       = "";
    $display    = "display:none;";
  }

  $sql   = "SELECT * FROM t_monitor WHERE monitor_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;
  $agency_code        = $row[0]['agency_code'];
  $monitor_name	      = $row[0]['monitor_name'];
  $ip_source          = $row[0]['ip_source'];
  $ip_target          = $row[0]['ip_target'];
  $monitor_desc       = $row[0]['monitor_desc'];
}
?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="monitor_id" value="<?=$id?>">
<div class="modal-body">
  <div class="row">
      <div class="col-md-8">
        <div class="form-group">
          <label>สำนักงานประกันสังคม</label>
          <select name="agency_code" class="form-control select2" style="width: 100%;" required>
            <option value="">&nbsp;</option>
          <?php
            $sqla   = "SELECT * FROM t_agency where is_active not in ('D') and agency_code <> '1000' order by agency_code";
            //echo $sqla;
            $querya      = DbQuery($sqla,null);
            $jsona       = json_decode($querya, true);
            $dataCounta  = $jsona['dataCount'];
            $rowA        = $jsona['data'];


            foreach ($rowA as $k => $value)
            {
                $agencyCode = $value['agency_code'];
                $agencyName = $value['agency_name'];
                $agencyName =  str_replace("สำนักงานประกันสังคม","",$agencyName);

                $selected = $agency_code==$agencyCode?"selected": "";

                echo "<option value=\"$agencyCode\" $selected >$agencyName</option>";
            }
          ?>
          </select>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>ชื่อเครื่อง</label>
          <input type="text" <?= $disabled ?> value="<?= $monitor_name ?>" name="monitor_name" class="form-control"  placeholder="" required>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label>รายละเอียด</label>
          <input type="text" <?= $disabled ?> value="<?= $monitor_desc ?>" name="monitor_desc" class="form-control"  placeholder="" >
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <label>IP Source</label>
          <input type="text" <?= $disabled ?> value="<?= $ip_source ?>" name="ip_source" class="form-control"  placeholder="192.168.x.xxx:80xx" required>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>IP Target </label>
          <input type="text" <?= $disabled ?> value="<?= $ip_target  ?>" name="ip_target" class="form-control"  placeholder="192.168.x.xxx:80xx" required>
        </div>
      </div>
    </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
  })
</script>
