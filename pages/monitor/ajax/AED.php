<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action       = isset($_POST['action'])?$_POST['action']:"";
$monitor_id   = isset($_POST['monitor_id'])?$_POST['monitor_id']:"";

$ip_source  = $_POST['ip_source'];
$ip_target  = $_POST['ip_target'];


//print_r($_POST);
unset($_POST["action"]);

if($action == "ADD")
{
    unset($_POST["monitor_id"]);
    $_POST["date_create"]     = date('Y/m/d H:i:s');
    $sql = DBInsertPOST($_POST,'t_monitor');
}
else if($action == "EDIT")
{
    $sql = DBUpdatePOST($_POST,'t_monitor','monitor_id');
}
else if($action == "DEL")
{
    $sql = "UPDATE t_monitor SET is_active = 'D' WHERE monitor_id = '$monitor_id'";
}

//echo $sql;
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($row['errorInfo'][0]) == 0)
{
  if($action == "ADD"){
    $monitor_id = $row['id'];
  }
  if($action != "DEL")
  {
      $filePath = "../../../novnc/websockity/".$monitor_id.".js";

      if (file_exists($filePath)) {
		  //echo $filePath;
        unlink($filePath);
      }

      $createFileJS=fopen($filePath,'w');

      $text = "var websockify = require('node-websockify');
                websockify({
                source: '".$ip_source."',
                target: '".$ip_target."'
              });";

      fwrite($createFileJS, $text);
      fclose($createFileJS);
  }


  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}



?>
