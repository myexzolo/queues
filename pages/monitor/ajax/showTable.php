<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/connectSignex.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px">ลำดับ</th>
      <th style="width:80px;">Monitor</th>
      <th>สำนักงานประกันสังคม</th>
      <th>ชื่อเครื่อง</th>
      <th style="width:90px;">สถานะเครื่อง</th>
      <th style="width:50px;">ดู</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:50px;">แก้ไข</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <th style="width:50px;">ลบ</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $agencyCode = $_POST['agencyCode'];

      $con = "";

      if($agencyCode != "")
      {
        $con = " and r.agency_code = '$agencyCode'";
      }


      $sqls   = "SELECT r.*, a.agency_name
                 FROM t_monitor r, t_agency a
                 where r.is_active = 'Y' and r.agency_code = a.agency_code $con
                 ORDER BY r.agency_code";

      //echo $sqls;
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      if($dataCount > 0)
      {
        foreach ($rows as $key => $value) {


          $is_active        = $value['is_active'];
          $monitor_id       = $value['monitor_id'];
          $monitor_name     = $value['monitor_name'];
          $password         = $value['password'];
          $agency_code      = $value['agency_code'];
          $ip_source        = $value['ip_source'];
          $ip_target        = $value['ip_target'];
          $monitor_desc     = $value['monitor_desc'];



          $activeTxt = "offline";
          $bg        = "bg-red";

          $onclick    = "";
          $disabled   = "";

          $sqlp       = "SELECT * FROM player where name = '$monitor_name'";

          $queryp     = DbQuerySignex($sqlp,null);
          $jsonp      = json_decode($queryp, true);
          $dataCountp = $jsonp['dataCount'];
          $rowp       = $jsonp['data'];

          $idDivStatus  = "";
          if($dataCountp > 0)
          {
            $last_active  = $rowp[0]['last_active'];
            $id           = $rowp[0]['id'];

            $idDivStatus  = "div_".$id;
            $dateNow      = date("Y-m-d H:i:s", strtotime('-7 hours'));

            $to_time    = strtotime($last_active);
            $from_time  = strtotime($dateNow);

            $diff =  round(abs($to_time - $from_time) / 60,2). " minute";

            // echo   $last_active." >> ".$dateNow." : ". $diff;

            if( $diff > 1.2)
            {
              $activeTxt = "offline";
              $bg        = "bg-red";
            }else{
              $activeTxt = "online";
              $bg        = "bg-green";
            }

          }else{
            $activeTxt = "unknown";
            $bg        = "bg-yellow";
          }


          $agency_name      =  str_replace("สำนักงานประกันสังคม","",$value['agency_name']);




    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td><a class="btn_point"><i class="fa fa-desktop" onclick="monitor('<?= $monitor_id?>','<?= $ip_source?>')"></i></a></td>
      <td align="left"><?=$agency_name ?></td>
      <td align="left"><?=$monitor_name ?></td>
      <td align="center">
        <div class="<?=$bg?> statusDevice" id="<?= $idDivStatus ?>"><?=$activeTxt ?></div>
      </label>
      </td>
      <td>
        <a class="btn_point text-green"><i class="fa fa-eye" onclick="showForm('VIEW','<?= $monitor_id?>')"></i></a>
      </td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?= $monitor_id?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="del('<?= $monitor_id ?>','<?=$monitor_name ?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php }} ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });

  })
</script>
