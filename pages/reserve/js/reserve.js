$(function () {
  pushMenu();
  showTable();
})


function pushMenu()
{
  if($( window ).width() < 1900){
     $('[data-toggle="push-menu"]').pushMenu('toggle');
  }
}


function showTable(){
  var agencyCode = $("#agencyCode").val();
  $.post("ajax/showTable.php",{agencyCode:agencyCode})
    .done(function( data ) {
      $('#showTable').html(data);
  });
}


function cancelReserve(tr_id,ref){
  $.smkConfirm({
    text:'ยืนยันการยกเลิกการจองรหัส : '+ ref +' ?',
    accept:'Yes',
    cancel:'No'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/AED.php",{action:'CANCEL',tr_id:tr_id})
        .done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#fff',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}
