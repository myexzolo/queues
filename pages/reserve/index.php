<!DOCTYPE html>
  <?php
  $pageName     = "รายการจองคิว Online";
  $pageCode     = "";
  ?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>ระบบสนับสุนนการให้บริการ (Queue) - <?= $pageName ?></title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/reserve.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php include("../../inc/sidebar.php"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              <?= $pageName ?>
              <small><?=$pageCode ?></small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="../../pages/home/"><i class="fa fa-home"></i> หน้าหลัก</a></li>
              <li class="active"><?= $pageName ?></li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php"); ?>
            <!-- Main row -->
            <div class="row">
              <div class="col-md-12">
                <div class="box box-success" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1);">
                  <div class="box-header with-border">
                    <h3 class="box-title"><span style="font-size: 20px;">สำนักงานประกันสังคม : <span></h3>
                    <select id="agencyCode" class="form-control select2" style="width: 350px;" onchange="showTable()">
                      <option value="">ทั้งหมด</option>
                    <?php
                      // $agencyCode = $_SESSION['AGENCY_CODE'];

                      $sqla   = "SELECT * FROM t_agency where is_active not in ('D') and agency_code <> '1000'";
                      //echo $sqla;

                      $querya      = DbQuery($sqla,null);
                      $jsona       = json_decode($querya, true);
                      $dataCounta  = $jsona['dataCount'];
                      $rowA        = $jsona['data'];


                      foreach ($rowA as $k => $value)
                      {
                          $agency_code = $value['agency_code'];
                          $agency_name = $value['agency_name'];
                          $agency_name =  str_replace("สำนักงานประกันสังคม","",$agency_name);

                          echo "<option value=\"$agency_code\">$agency_name</option>";
                      }
                    ?>
                    </select>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div id="showTable"></div>
                  </div>
                </div>
              </div>
            </div>


            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">จัดการสมาชิก</h4>
                  </div>
                  <form id="formAED" novalidate enctype="multipart/form-data">
                  <!-- <form novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
                    <div id="show-form"></div>
                  </form>
                </div>
              </div>
            </div>
            <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/reserve.js"></script>
    </body>
  </html>
