<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px">ลำดับ</th>
      <th>รหัสการจอง</th>
      <th>วันที่การจอง</th>
      <th>ชื่่อ - นามสกุล</th>
      <th>สำนักงานประกันสังคม</th>
      <th style="width:90px">สถานะ</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:100px;">ยกเลิก</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $agencyCode = $_POST['agencyCode'];

      $con = "";

      if($agencyCode != "")
      {
        $con = " and r.agency_code = '$agencyCode'";
      }

      $sqls   = "SELECT r.*, a.agency_name ,m.mem_name, m.mem_last
                 FROM t_reserve r, t_agency a, t_member m
                 where r.is_active = 'Y' and r.agency_code = a.agency_code and 	r.mem_id = 	m.mem_id $con
                 ORDER BY date_reserve DESC";

      //echo $sqls;
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      if($dataCount > 0)
      {
        foreach ($rows as $key => $value) {

          $hide_list = trim($value['hide_list']);
          $value = checkConsent($value,$hide_list,'H');// F = ทั้งหมด , H = ครึ่ง , ตัวเลข

          $is_active        = $value['is_active'];
          $tr_id            = $value['tr_id'];
          $agency_code      = $value['agency_code'];
          $status_reserve   = $value['status_reserve'];
          $memname          = $value['mem_name']." ".$value['mem_last'];

          $agency_name      =  str_replace("สำนักงานประกันสังคม","",$value['agency_name']);

          $date_reserve     = DateThai($value['date_reserve'])." ".$value['time_start']." - ".$value['time_end'];

          $dateR =  $value['date_reserve']." ".$value['time_end'].":00";

          $datetime1 = new DateTime();
          $datetime2 = new DateTime($dateR);

          if( $datetime1 > $datetime2 )
          {
              $status_reserve = "E";
              $sql = "UPDATE t_reserve SET status_reserve = 'E' WHERE tr_id = '$tr_id'";
              DbQuery($sql,null);

              $dataR['action']          = "U";
              $dataR['status_reserve']  = "E";
              $dataR['ref_code']        = $value['ref_code'];
              $dataR['agency_code']     = $value['agency_code'];
              $dataR['status_send']     = $value['status_send'];
              $dataR['ref_queue']       = $value['ref_queue'];


              $ipAgency    = getIPbyAgency($agency_code);
              $data_array  = array(
                                 "functionName" => "manageReserve",  ///แก้ ชื่อ Service
                                 "dataJson" => $dataR,
                               );

              $url  = "http://$ipAgency/ws/service.php";

              // echo $url;
              //echo json_encode($data_array);

              $make_call = callAPI('POST', $url, json_encode($data_array));
              $response = json_decode($make_call, true);
              $status   = $response['status'];
              $data     = $response['data'];

              $arrUpdate['tr_id']        =  $tr_id; //แก้ ID
              $arrUpdate['ref_code']     =  $dataR["ref_code"];
              $arrUpdate['status_send']  =  "N";

              // print_r($response);
              $sql = "";
              if($status == "200")
              {
                  $arrUpdate['status_send']  =  "S";
                  ///แก้ ชื่อ table
              }else{
                  $arrLog['url']  = $url;
                  $arrLog['data'] = json_encode($data_array);
                  $arrLog['table_name']   = 't_reserve';
                  $arrLog['id_update']    = 'tr_id';
                  $arrLog['date_create']  = date('Y/m/d H:i:s');
                  $arrLog['data_update']  = json_encode($arrUpdate);

                  $sql = DBInsertPOST($arrLog,'t_log_send_service'); ///แก้ ชื่อ table
              }

              $sql .= DBUpdatePOST($arrUpdate,'t_reserve','tr_id');
              DbQuery($sql,null);
          }

          $activeTxt  = "";
          $bg         = "";
          $onclick    = "";
          $disabled   = "";



          if($status_reserve == "R")
          {
            $activeTxt = "จองคิว";
            $bg        = "bg-green-active";
          }else if($status_reserve == "S"){
            $activeTxt = "รับบริการแล้ว";
            $bg        = "bg-navy";
          }else if($status_reserve == "C"){
            $activeTxt = "ยกเลิก";
            $bg        = "bg-gray";
          }else if($status_reserve == "E"){
            $activeTxt = "หมดอายุ";
            $bg        = "bg-gray";
          }

    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="center"><?=$value['ref_queue'] ?></td>
      <td align="center"><?=$date_reserve ?></td>
      <td align="left"><?=$memname ?></td>
      <td align="left"><?=$agency_name ?></td>
      <td><div class="<?=$bg?>"><?=$activeTxt ?></div></td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point text-yellow"><i class="ion ion-android-cancel" onclick="cancelReserve('<?=$tr_id?>','<?=$value['ref_queue']?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php }} ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });
  })
</script>
