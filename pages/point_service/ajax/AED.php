<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action           = isset($_POST['action'])?$_POST['action']:"";
$point_id         = isset($_POST['point_id'])?$_POST['point_id']:"";
$service_id_list  = isset($_POST['service_id_list'])?$_POST['service_id_list']:"";


//print_r($_POST);
unset($_POST["action"]);
if($service_id_list != "")
{
  $_POST["service_id_list"] = implode(",",$service_id_list);
}

$_POST["status_send"] = "N"; //เพิ่ม


$_POST['is_logo']           = isset($_POST['is_logo'])?"Y":"N";
$_POST['is_num_service']    = isset($_POST['is_num_service'])?"Y":"N";
$_POST['is_etimate_time']   = isset($_POST['is_etimate_time'])?"Y":"N";
$_POST['is_welcome']        = isset($_POST['is_welcome'])?"Y":"N";
$_POST['is_agency']         = isset($_POST['is_agency'])?"Y":"N";
$_POST['is_datetime']       = isset($_POST['is_datetime'])?"Y":"N";
$_POST['is_message']        = isset($_POST['is_message'])?"Y":"N";
$_POST['is_channel']        = isset($_POST['is_channel'])?"Y":"N";
$_POST['is_service_name']   = isset($_POST['is_service_name'])?"Y":"N";



if($action == "ADD")
{
    unset($_POST["point_id"]);
    $_POST["date_create"] = date('Y/m/d H:i:s');
    $sql = DBInsertPOST($_POST,'t_point_service');
}
else if($action == "EDIT")
{
    $sql = DBUpdatePOST($_POST,'t_point_service','point_id');
}
else if($action == "DEL")
{
    $sql = "UPDATE t_point_service SET is_active = 'D', status_send = 'N' WHERE point_id = '$point_id'";
}

// echo $sql;
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($row['errorInfo'][0]) == 0)
{
  if($action == "ADD")
  {
    $point_id = $row['id']; ///แก้ ID
  }

  $sqls   = "SELECT * FROM t_point_service where point_id = '$point_id'"; //เปลี่ยน table
  $querys     = DbQuery($sqls,null);
  $json       = json_decode($querys, true);
  $rows       = $json['data'][0];

  $rows["action"]   = "U";
  $agency_code = $rows["agency_code"];

  if($action == 'ADD')
  {
    $rows["ref_code"] = $agency_code."#C".$point_id;   ///แก้ ID
    $rows["action"]   = "ADD";
  }

  $rows = chkDataEmpty($rows);

  $ipAgency    = getIPbyAgency($agency_code);
  $data_array  = array(
                     "functionName" => "managePointService",  ///แก้ ชื่อ Service
                     "dataJson" => $rows,
                   );

  $url  = "http://$ipAgency/ws/service.php";

  // echo $url;
  // echo json_encode($data_array);

  $make_call = callAPI('POST', $url, json_encode($data_array));
  $response = json_decode($make_call, true);
  $status   = $response['status'];
  $data     = $response['data'];

  $arrUpdate['point_id']     =  $point_id; //แก้ ID
  $arrUpdate['ref_code']     =  $rows["ref_code"];
  $arrUpdate['status_send']  =  "N";

  // print_r($response);
  $sql = "";
  if($status == "200")
  {
      $arrUpdate['status_send']  =  "S";
      ///แก้ ชื่อ table
  }else{
      $arrLog['url']  = $url;
      $arrLog['data'] = json_encode($data_array,JSON_UNESCAPED_UNICODE);
      $arrLog['table_name']   = 't_point_service';
      $arrLog['id_update']    = 'point_id';
      $arrLog['date_create']  = date('Y/m/d H:i:s');
      $arrLog['data_update']  = json_encode($arrUpdate);

      $sql = DBInsertPOST($arrLog,'t_log_send_service'); ///แก้ ชื่อ table
  }

  $sql .= DBUpdatePOST($arrUpdate,'t_point_service','point_id');
  DbQuery($sql,null);

  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}



?>
