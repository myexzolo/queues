<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$agencyCode = $_SESSION['AGENCY_CODE'];

$point_number     = "";
$point_detail     = "";
$message_queue    = "";
$is_active        = "";
$point_open       = "";
$point_close      = "";
$language_display = "";


$is_logo          = "";
$is_num_service   = "";
$is_etimate_time  = "";
$is_welcome       = "";
$is_agency        = "";
$is_datetime      = "";
$is_message       = "";
$is_channel       = "";
$is_service_name  = "";
$service_id_list  = "";

$disabled   = "";
$disabled2  = "disabled";
$bg         = "background-color:#fff;";
$display    = "";

$ldt = "checked";
$lde = "";

if($action == 'ADD')
{
  $disabled2  = "";
}


if($action == 'EDIT' || $action == 'VIEW')
{
  if($action == 'VIEW')
  {
    $disabled = "disabled";
    $bg       = "";
    $display    = "display:none;";
  }

  $sql   = "SELECT * FROM t_point_service WHERE point_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;
  $point_number     = $row[0]['point_number'];
  $point_detail     = $row[0]['point_detail'];
  $message_queue    = $row[0]['message_queue'];
  $language_display = $row[0]['language_display'];
  $is_active        = $row[0]['is_active'];
  $point_open       = $row[0]['point_open'];
  $point_close      = $row[0]['point_close'];
  $service_id_list  = !empty(@$row[0]['service_id_list'])?$row[0]['service_id_list']:"";

  $is_logo          = $row[0]['is_logo'] == 'Y'?"checked":"";
  $is_num_service   = $row[0]['is_num_service'] == 'Y'?"checked":"";
  $is_etimate_time  = $row[0]['is_etimate_time'] == 'Y'?"checked":"";
  $is_welcome       = $row[0]['is_welcome'] == 'Y'?"checked":"";
  $is_agency        = $row[0]['is_agency'] == 'Y'?"checked":"";
  $is_datetime      = $row[0]['is_datetime'] == 'Y'?"checked":"";
  $is_message       = $row[0]['is_message'] == 'Y'?"checked":"";
  $is_channel       = $row[0]['is_channel'] == 'Y'?"checked":"";
  $is_service_name  = $row[0]['is_service_name'] == 'Y'?"checked":"";


  if($language_display == "en")
  {
    $ldt = "";
    $lde = "checked";
  }



}

  // $optionProvince = getoptionProvince($provice_code);
?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="point_id" value="<?=$id?>">
<input type="hidden" name="agency_code" value="<?=$agencyCode?>">
<div class="modal-body">
  <div class="row">
      <div class="col-md-2">
        <div class="form-group">
          <label>จุดบริการที่</label>
          <input type="text" <?= $disabled ?> OnKeyPress="return chkNumber(this)" value="<?= $point_number ?>" name="point_number" class="form-control"  placeholder="" required>
        </div>
      </div>
      <div class="col-md-10">
        <div class="form-group">
          <label>รายละเอียด</label>
          <input type="text" <?= $disabled ?> value="<?= $point_detail ?>" name="point_detail" class="form-control" placeholder="รายละเอียด" >
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>เสียงเรียกคิว</label>
          <div class="radio">
              <label>
                <input type="radio" <?= $disabled ?> name="language_display" value="th" <?=$ldt?> required>
                ภาษาไทย
              </label>
              &nbsp;&nbsp;&nbsp;
              <label>
                <input type="radio" <?= $disabled ?> name="language_display" value="en" <?=$lde?> required>
                ภาษาอังกฤษ
              </label>
          </div>
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label>เวลาเปิดบริการ</label>
          <input type="text" <?= $disabled ?> value="<?= $point_open ?>" name="point_open" class="form-control" placeholder="00:00" required>
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label>เวลาปิดบริการ</label>
          <input type="text" <?= $disabled ?> value="<?= $point_close ?>" name="point_close" class="form-control" placeholder="00:00" required>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>สถานะ</label>
          <select name="is_active" <?= $disabled ?> class="form-control " style="width: 100%;" required >
            <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
            <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
          </select>
        </div>
      </div>
    </div>
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">การตั้งค่าออกแบบการแสดงบัตรคิว</h3>
        </div>
        <div class="box-body">
          <div class="col-md-4">
            <div class="checkbox">
                <label>
                  <input type="checkbox" name="is_logo"  value="Y" <?=$is_logo?>>
                  แสดง Logo หน่วยงาน
                </label>
            </div>
          </div>
          <div class="col-md-4">
            <div class="checkbox">
                <label>
                  <input type="checkbox" name="is_welcome"  value="Y" <?=$is_welcome?>>
                  แสดงข้อความ ยินดีตอนรับ
                </label>
            </div>
          </div>
          <div class="col-md-4">
            <div class="checkbox" >
                <label>
                  <input type="checkbox" name="is_agency"  value="Y" <?=$is_agency?>>
                  แสดงชื่อหน่วยงาน
                </label>
            </div>
          </div>
          <div class="col-md-4">
            <div class="checkbox" >
                <label>
                  <input type="checkbox" name="is_service_name"  value="Y" <?=$is_service_name?>>
                  แสดงงานบริการ
                </label>
            </div>
          </div>
          <div class="col-md-4">
            <div class="checkbox">
                <label>
                  <input type="checkbox" name="is_message"  value="Y" <?=$is_message?>>
                  แสดงข้อความ
                </label>
            </div>
          </div>
          <div class="col-md-4">
            <div class="checkbox">
                <label>
                  <input type="checkbox" name="is_num_service"  value="Y" <?=$is_num_service?>>
                  แสดงจำนวนผู้รับบริการ
                </label>
            </div>
          </div>
          <div class="col-md-4">
            <div class="checkbox">
                <label>
                  <input type="checkbox" name="is_channel"  value="Y" <?=$is_channel?>>
                  แสดงช่องบริการ
                </label>
            </div>
          </div>
          <div class="col-md-4">
            <div class="checkbox">
                <label>
                  <input type="checkbox" name="is_datetime"  value="Y" <?=$is_datetime?>>
                  แสดงเวลารับบริการ
                </label>
            </div>
          </div>
          <div class="col-md-4">
            <div class="checkbox">
                <label>
                  <input type="checkbox" name="is_etimate_time"  value="Y" <?=$is_etimate_time?>>
                  แสดงเวลาที่คาดจะได้รับ
                </label>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>ข้อความแสดงบนบัตรคิว</label>
              <textarea <?= $disabled ?>  name="message_queue" rows="2" class="form-control"><?= $message_queue ?></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">งานบริการ</h3>
        </div>
        <div class="box-body">
          <?php
            $sqlc   = "SELECT *
                       FROM t_cat_service
                       where is_active = 'Y'
                       ORDER BY division_id , cat_service_id";

            //echo $sqls;
            $queryc       = DbQuery($sqlc,null);
            $jsonc        = json_decode($queryc, true);
            $errorInfoc   = $jsonc['errorInfo'];
            $dataCountc   = $jsonc['dataCount'];
            $rowc         = $jsonc['data'];

            if($dataCountc > 0)
            {
              foreach ($rowc as $keyc => $valuec)
               {
                  $cat_service_id = $valuec['cat_service_id'];
              ?>
              <div class="col-md-12">
                  <label><?=$valuec['cat_service_name']; ?></label>
              </div>
            <?php

              $sqls   = "SELECT *
                         FROM t_service_agency
                         where is_active = 'Y' AND cat_service_id = '$cat_service_id' and agency_code = '$agencyCode'
                         ORDER BY cat_service_id , service_code_a +'' ";

              //echo $sqls;
              $querys     = DbQuery($sqls,null);
              $json       = json_decode($querys, true);
              $errorInfo  = $json['errorInfo'];
              $dataCount  = $json['dataCount'];
              $rows       = $json['data'];

              $servicelist = array();
              if($service_id_list != "")
              {
                $servicelist = explode(",",$service_id_list);
              }

              if($dataCount > 0)
              {
                foreach ($rows as $key => $value)
                {
                  $service_agency_id = $value['service_agency_id'];
                  $service_id        = $value['service_id'];
                  $checked = "";
                  if(in_array($service_id, $servicelist))
                  {
                    $checked = "checked";
                  }
              ?>
              <div class="col-md-6">
                <div class="checkbox">
                    <label>
                      <input type="checkbox" name="service_id_list[]"  value="<?=$service_id ?>" <?=$checked ?> >
                      <?=$value['service_name_a']?>
                    </label>
                </div>
              </div>
          <?php
              }
          }
        }
      }
        ?>
        </div>

      <!-- /.box-body -->
    </div>
        <!-- /.row -->
    <!-- </div> -->
  <!-- </div> -->
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
  })
</script>
