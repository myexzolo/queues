<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th>No.</th>
      <th>User Login</th>
      <th>ชื่อ - สกุล (ภาษาไทย)</th>
      <th>ชื่อ - สกุล (ภาษาอังกฤษ)</th>
      <th>สิทธิการเข้าใช้ระบบ</th>
      <th>หน่วยงาน</th>
      <th>สถานะ</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:100px;">Re Password</th>
      <th style="width:50px;">Edit</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <th style="width:50px;">Del</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $str            = $_SESSION['member'][0]['user_id'] == 0 ?"":" and user_id > 0 ";
      $agency_code    = $_SESSION['AGENCY_CODE'];
      if($agency_code != "1000")
      {
          $str .= " and u.agency_code = '$agency_code'";
      }
      //print_r($_SESSION['member'][0]);
      $sqls   = "SELECT u.* , a.agency_name
                 FROM t_user u
                 LEFT JOIN t_agency a  ON u.agency_code = a.agency_code
                 where u.is_active  not in ('D','R','C')
                 and type_user = 'ADMIN' $str
                 ORDER BY u.user_id DESC";

      //echo $sqls;
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

        foreach ($rows as $key => $value) {

          $role_list = $value['role_list'];

          $roleName = "";
          $sqlr = "SELECT role_name FROM t_role where  role_id in ($role_list) ORDER BY role_id";

          $queryr     = DbQuery($sqlr,null);
          $jsonr      = json_decode($queryr, true);
          $dataCountr = $jsonr['dataCount'];

          if($dataCountr > 0){
              $rowr       = $jsonr['data'];
              foreach ($rowr as $keyr => $valuer) {
                if($roleName == "")
                {
                  $roleName = $valuer['role_name'];
                }else
                {
                  $roleName .= "<br>".$valuer['role_name'];
                }
              }
          }
    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="left"><?=$value['user_login']?></td>
      <td align="left"><?=$value['user_name']." ".$value['user_last']?></td>
      <td align="left"><?=$value['user_name_en']." ".$value['user_last_en']?></td>
      <td align="left"><?= $roleName ?></td>
      <td align="left"><?=@$value['agency_name']?></td>
      <td><?=$value['is_active']=='Y'?"ใช้งาน":"ไม่ใช้งาน";?></td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point text-yellow"><i class="fa fa-key" onclick="resetPass('<?=$value['user_id']?>')"></i></a>
      </td>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['user_id']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="del('<?=$value['user_id']?>','<?=$value['user_login']?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
    <?php } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
