<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action         = isset($_POST['action'])?$_POST['action']:"";
$user_id        = isset($_POST['user_id'])?$_POST['user_id']:"";
$user_login     = isset($_POST['user_login'])?$_POST['user_login']:"";
$user_name      = isset($_POST['user_name'])?$_POST['user_name']:"";
$user_last      = isset($_POST['user_last'])?$_POST['user_last']:"";
$user_name_en   = isset($_POST['user_name_en'])?$_POST['user_name_en']:"";
$user_last_en   = isset($_POST['user_last_en'])?$_POST['user_last_en']:"";
$user_email     = isset($_POST['user_email'])?$_POST['user_email']:"";
$note1          = isset($_POST['note1'])?$_POST['note1']:"";
$note2          = isset($_POST['note2'])?$_POST['note2']:"";
$password       = isset($_POST['user_password'])?$_POST['user_password']:"";
$is_active      = isset($_POST['is_active'])?$_POST['is_active']:"";
$role_list      = @implode(",",isset($_POST['role_list'])?$_POST['role_list']:"");
$agency_code    = isset($_POST['agency_code'])?$_POST['agency_code']:"";
$branch_code    = isset($_POST['branch_code'])?$_POST['branch_code']:"";
$department_id  = isset($_POST['department_code'])?$_POST['department_code']:"";

$user_password = @md5($password);

$user_img   = "";
$updateImg  = "";

$user_id_update = $_SESSION['member'][0]['user_id'];

$path = "../../../image/user/";
if(isset($_FILES["user_img"])){
  //$user_img = resizeImageToBase64($_FILES["user_img"],'256','256','100',$user_id_update,$path);

  $user_img = resizeImageToUpload($_FILES["user_img"],'256','256',$path,$user_login);

  if($action == "EDIT" && $user_img != "")
  {
    $updateImg = "user_img = '$user_img',";
  }
}


// --ADD EDIT DELETE Module-- //
if(empty($user_id) && $action == 'ADD'){
  $sql = "INSERT INTO t_user
         (user_login,user_name,user_last,user_password,is_active,role_list,
           user_img,user_id_update,note1,note2,pw,type_user,agency_code,status_send,user_name_en,user_last_en)
         VALUES('$user_login','$user_name','$user_last','$user_password','$is_active','$role_list',
           '$user_img','$user_id_update','$note1','$note2','$password','ADMIN','$agency_code','N','$user_name_en','$user_last_en')";
}else if($action == 'EDIT'){
    $sql = "UPDATE t_user SET
            user_login      = '$user_login',
            user_name       = '$user_name',
            user_last       = '$user_last',
            user_name_en    = '$user_name_en',
            user_last_en    = '$user_last_en',
            is_active       = '$is_active',
            role_list       = '$role_list',
            $updateImg
            note1           = '$note1',
            note2           = '$note2',
            agency_code     = '$agency_code',
            status_send     = 'N',
            user_id_update  = '$user_id_update'
            WHERE user_id   = $user_id";

    if($user_id_update == $user_id){
      $sqls   = "SELECT * FROM t_user WHERE user_id = '$user_id'";
      $query      = DbQuery($sqls,null);
      $json       = json_decode($query, true);
      $rows       = $json['data'];

      $date_login = $_SESSION['member'][0]['date_login'];
      $rows[0]['date_login'] = $date_login;
      $_SESSION['member'] = $rows;
    }

}else if($action == 'RESET'){
  $dateNow  =  date('Y/m/d H:i:s');
  $sql = "UPDATE t_user SET
            user_password  = '$user_password',
            pw  = '$password',
            date_update    =  '$dateNow',
            status_send    = 'N'
            WHERE user_id  = '$user_id'";
}else if($action == "DEL"){
  $sql = "UPDATE t_user SET is_active = 'D', status_send  = 'N'  WHERE user_id = '$user_id'";
}

//echo $sql;

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
//print_r($row);
$errorInfo  = $json['errorInfo'];
//echo intval($errorInfo[0]);
if(intval($errorInfo[0]) == 0)
{
  if($action == 'ADD')
  {
    $user_id = $json['id'];
  }

  $sqls       = "SELECT * FROM t_user WHERE user_id = '$user_id'";
  $query      = DbQuery($sqls,null);
  $json       = json_decode($query, true);
  $rows       = $json['data'][0];

  $rows["action"]   = "U";

  if($action == 'ADD')
  {
    $rows["ref_code"] = $agency_code."#C".$user_id;   ///แก้ ID
    $rows["action"]   = "ADD";
  }else{
    $agency_code = $rows["agency_code"];
  }

  if($agency_code != "1000")
  {
    $rows  = chkDataEmpty($rows);

    $ipAgency     = getIPbyAgency($agency_code);
    $url          = "http://$ipAgency/ws/service.php";
    $data_array   = array(
                       "functionName" => "setUser",
                       "dataJson" => $rows,
                     );

    //print_r(json_encode($data_array));

    $make_call_user  = callAPI('POST', $url, json_encode($data_array));
    $responseUser    = json_decode($make_call_user, true);
    $statusUser     = $responseUser['status'];

    $arrUpdate['user_id']    =  $user_id;
    $arrUpdate['ref_code']   =  $rows["ref_code"];

    if($statusUser == "200")
    {
        $arrUpdate['status_send']     =  "S";
        $sql = DBUpdatePOST($arrUpdate,'t_user','user_id');
    }else{
        $arrLog['url']  = $url;
        $arrLog['data'] = json_encode($data_array,JSON_UNESCAPED_UNICODE);
        $arrLog['table_name'] = 't_user';
        $arrLog['id_update']  = 'user_id';
        $arrLog['data_update']  = json_encode($arrUpdate);
        $arrLog['date_create']  = date('Y-m-d H:i:s');

        $sql = DBInsertPOST($arrLog,'t_log_send_service'); ///แก้ ชื่อ table

    }
    //echo  $sql;
    DbQuery($sql,null);

  }


  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}


?>
