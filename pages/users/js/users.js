
showTable();

function showTable(){
  $.get( "ajax/showTable.php")
  .done(function( data ) {
    $("#showTable").html( data );
  });
}

function showForm(value="",id=""){
  $.post("ajax/formUsers.php",{value:value,id:id})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}


function checkUserCode(){
  var text = $('#user_login').val();
  $.post("ajax/checkUserCode.php",{UserCode:text})
  .done(function( data ) {
        console.log(data);
      if(data.status)
      {
        $.smkAlert({
          text: 'User Login ซ้ำ !!',
          type: 'danger',
          position:'top-center'
        });
        $('#user_login').val('');
        $('#user_login').focus();
      }
    });
  }

function readURL(input,values) {
  if (input.files) {
      var filesAmount = input.files.length;
      $('#'+values).html('');
      for (i = 0; i < filesAmount; i++) {
          checkTypeImage(input,values);
          var reader = new FileReader();
          reader.onload = function(event) {
              $($.parseHTML("<img style='height: 60px;'>")).attr('src', event.target.result).appendTo('#'+values);
          }
          reader.readAsDataURL(input.files[i]);
      }

  }
}

function checkTypeImage(input,values){
  var file = input.files[0];
  var fileType = file["type"];
  var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
  if ($.inArray(fileType, validImageTypes) < 0) {
      alert('ประเภทไฟล์ไม่ถูกต้อง');
      $('#'+values).html('');
      input.value = '';
  }
}

function CreatePass(){
  var pass = GenPass();
  $('#pass1').val(pass);
  $('#pass2').val(pass);
}

function resetPass(id){
  $.post("ajax/formResetPass.php",{id:id})
    .done(function( data ) {
      $('#myModalReset').modal({backdrop:'static'});
      $('#show-form-rw').html(data);
  });
}

function exportExcel()
{
   postURL_blank("../../template/user_excel_template.xlsx");
}

function upload(){
  $.get("ajax/formUpload.php")
  .done(function( data ) {
      $('#myModal2').modal({backdrop:'static'});
      $("#formShow2").html(data);
  });
}

function showHistoryUpload(){
  $.get("ajax/showHisUpload.php")
  .done(function( data ) {
      $("#historyUpload").html(data);
  });
}


function del(id,name){
  $.smkConfirm({
    text:'ยืนยันการลบข้อมูล User : '+ name +' ?',
    accept:'Yes',
    cancel:'No'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/AEDUsers.php",{action:'DEL',user_id:id})
        .done(function( data ) {
          //console.log(data);
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#fff',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}


$('#formResetPassword').on('submit', function(event) {
  event.preventDefault();
  var action = $('#action').val();
  if ($('#formResetPassword').smkValidate()) {
      if( $.smkEqualPass('#pass1', '#pass2') ){
          $.ajax({
              url: 'ajax/AEDUsers.php',
              type: 'POST',
              data: new FormData( this ),
              processData: false,
              contentType: false,
              dataType: 'json'
          }).done(function( data ) {
            console.log(data);
            $.smkProgressBar({
              element:'body',
              status:'start',
              bgColor: '#fff',
              barColor: '#242d6d',
              content: 'Loading...'
            });
            setTimeout(function(){
              $.smkProgressBar({status:'end'});
              $('#formResetPassword').smkClear();
              showTable();
              showSlidebar();
              $.smkAlert({text: data.message,type: data.status});
              $('#myModalReset').modal('toggle');
            }, 1000);
          });
      }
  }
});

$('#formDataUpload').on('submit', function(event) {
  event.preventDefault();
  $('#loading').removeClass("none");
  $('#loading').addClass('loading');
  if ($('#formDataUpload').smkValidate()) {
    $.ajax({
        url: 'ajax/manageUpload.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function(data) {
        showHistoryUpload();
        $('#loading').removeClass('loading');
        $('#loading').addClass('none');
        showTable();
    }).fail(function (jqXHR, exception) {
        alert("ไม่สามารถนำเข้าได้");
        $('#loading').removeClass('loading');
        $('#loading').addClass('none');
    });
  }else{
    $('#loading').removeClass('loading');
    $('#loading').addClass('none');
  }
});

$('#formAddUsers').on('submit', function(event) {
  event.preventDefault();
  var action = $('#action').val();
  if ($('#formAddUsers').smkValidate()) {

    if(action == 'ADD'){
        if( $.smkEqualPass('#pass1', '#pass2') ){
          // Code here
            $.ajax({
                url: 'ajax/AEDUsers.php',
                type: 'POST',
                data: new FormData( this ),
                processData: false,
                contentType: false,
                dataType: 'json'
            }).done(function( data ) {
              $.smkProgressBar({
                element:'body',
                status:'start',
                bgColor: '#ecf0f5',
                barColor: '#242d6d',
                content: 'Loading...'
              });
              setTimeout(function(){
                $.smkProgressBar({status:'end'});
                $('#formAddUsers').smkClear();
                showTable();
                showSlidebar();
                $.smkAlert({text: data.message,type: data.status});
                $('#myModal').modal('toggle');
              }, 1000);
            });
        }
    }else{
        $.ajax({
            url: 'ajax/AEDUsers.php',
            type: 'POST',
            data: new FormData( this ),
            processData: false,
            contentType: false,
            dataType: 'json'
        }).done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#ecf0f5',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            $('#formAddUsers').smkClear();
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
            $('#myModal').modal('toggle');
          }, 1000);
        });
    }



  }
});
