$(function () {
  pushMenu();
  showTable();
})


function pushMenu()
{
  if($( window ).width() < 1900){
     $('[data-toggle="push-menu"]').pushMenu('toggle');
  }
}


function showTable(){
  $.get("ajax/showTable.php")
    .done(function( data ) {
      $('#showTable').html(data);
  });
}


function showTablePrivate(){
  var consent_id = $("#consent_id").val();
  $.post("ajax/showTablePrivate.php",{consent_id:consent_id})
    .done(function( data ) {
      $('#showTablePrivate').html(data);
  });
}

function showForm(action,id){
  $.post("ajax/form.php",{action:action,id:id})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}

function showFormPrivate(id){
  $.post("ajax/formPrivate.php",{id:id})
    .done(function( data ) {
      $('#myModalPrivate').modal({backdrop:'static'});
      $('#show-form-private').html(data);
  });
}



function del(id,name){
  $.smkConfirm({
    text:'ยืนยันการลบข้อมูล หน่วยงาน : '+ name +' ?',
    accept:'Yes',
    cancel:'No'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/AED.php",{action:'DEL',consent_id:id})
        .done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#fff',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

function delPrivate(consent_data_id){
  //console.log("xxxx");
  $.post("ajax/AEDPrivate.php",{action:'DEL',consent_data_id:consent_data_id})
    .done(function( data ) {
        $.smkProgressBar({status:'end'});
        showTablePrivate();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
  });
}

function checknumber(inputs){
  var valid = /^\d{0,10}(\.\d{0,2})?$/.test(inputs.value),
      val = inputs.value;
  if(!valid){
      inputs.value = val.substring(0, val.length - 1);
  }
}

function chkNumber(ele)
{
    var vchar = String.fromCharCode(event.keyCode);
    if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
    ele.onKeyPress=vchar;
}


$('#formAED').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAED').smkValidate()) {
    var detail = dataEditor.getData();
    $('#consent_desc').val(detail);
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAED').smkClear();
        showTable();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});


$('#formAEDPrivate').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAEDPrivate').smkValidate()) {
    var consent_id = $("#consent_id").val();
    var action = $("#action").val();
    $.ajax({
        url: 'ajax/AEDPrivate.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
        $.smkProgressBar({status:'end'});
        $('#formAEDPrivate').smkClear();
        $("#consent_id").val(consent_id);
        $("#action").val(action);
        showTablePrivate();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
    });
  }
});


var init = ( function() {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get( 'bbcode' );

	return function() {
		var editorElement = CKEDITOR.document.getById( 'editor' );
		// :(((
		// if ( isBBCodeBuiltIn ) {
		// 	editorElement.setHtml(
		// 		'Hello world!\n\n' +
		// 		'I\'m an instance of [url=https://ckeditor.com]CKEditor[/url].'
		// 	);
		// }

		// Depending on the wysiwygarea plugin availability initialize classic or inline editor.
		if ( wysiwygareaAvailable ) {
      dataEditor = CKEDITOR.replace('editor', {
        toolbar: [{
            name: 'clipboard',
            items: ['PasteFromWord', '-', 'Undo', 'Redo']
          },
          {
            name: 'basicstyles',
            items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'Subscript', 'Superscript']
          },
          {
            name: 'links',
            items: ['Link', 'Unlink']
          },
          {
            name: 'paragraph',
            items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
          },
          {
            name: 'insert',
            items: ['Image', 'Table']
          },
          {
            name: 'editing',
            items: ['Scayt']
          },
          '/',
          {
            name: 'styles',
            items: ['Format', 'Font', 'FontSize']
          },
          {
            name: 'colors',
            items: ['TextColor', 'BGColor', 'CopyFormatting']
          },
          {
            name: 'align',
            items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
          },
          {
            name: 'document',
            items: ['Templates', 'PageBreak', 'Source']
          }
        ],
      // extraPlugins: 'uploadimage,image2,autogrow',
      // autoGrow_minHeight: 200,
      // autoGrow_maxHeight: 600,
      // autoGrow_bottomSpace: 50,
      // removePlugins: 'resize',
      // Configure your file manager integration. This example uses CKFinder 3 for PHP.
      filebrowserBrowseUrl: '../../ckfinder/ckfinder.html',
      filebrowserImageBrowseUrl: '../../ckfinder/ckfinder.html?type=Images',
      // filebrowserUploadUrl: '../../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
      //filebrowserUploadUrl: 'base64',
      filebrowserImageUploadUrl: '../../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

      // image2_alignClasses: [ 'image-align-left', 'image-align-center', 'image-align-right' ],
			// image2_disableResizer: true
    });
		} else {
			editorElement.setAttribute( 'contenteditable', 'true' );
			CKEDITOR.inline( 'editor' );

			// TODO we can consider displaying some info box that
			// without wysiwygarea the classic editor may not work.
		}
	};

	function isWysiwygareaAvailable() {
		// If in development mode, then the wysiwygarea must be available.
		// Split REV into two strings so builder does not replace it :D.
		if ( CKEDITOR.revision == ( '%RE' + 'V%' ) ) {
			return true;
		}
		return !!CKEDITOR.plugins.get( 'wysiwygarea' );
	}
} )();
