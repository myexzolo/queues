<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$id     = isset($_POST['id'])?$_POST['id']:"";

$consent_name   = "";
$consent_desc   = "";
$is_active      = "";


$disabled   = "";
$disabled2  = "disabled";
$bg         = "background-color:#fff;";
$display    = "";



?>
<input type="hidden" name="consent_id" id="consent_id" value="<?=$id?>">
<input type="hidden" name="action" id="action" value="ADD">
<div class="modal-body">
  <div class="box box-success" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1);">
    <div class="box-header with-border">
      <h3 class="box-title">รายละเอียดข้อมูล</h3>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label>รายการ</label>
            <input type="text"  value="" name="consent_data_name" class="form-control" placeholder="รายการ" required="">
          </div>
        </div>
        <div class="col-md-12">
            <label>ปกปิดข้อมูลส่วนบุคคล</label>
        </div>
        <div class="col-md-6">
          <div class="checkbox">
            <label>
              <input type="checkbox" name="hide_list[]" value="mem_name" data-smk-msg="&nbsp;">ชื่อ</label>
          </div>
        </div>
        <div class="col-md-6">
          <div class="checkbox">
            <label>
              <input type="checkbox" name="hide_list[]" value="mem_last" data-smk-msg="&nbsp;">นามสกุล</label>
          </div>
        </div>
        <div class="col-md-6">
          <div class="checkbox">
            <label>
              <input type="checkbox" name="hide_list[]" value="mem_idcard" data-smk-msg="&nbsp;">เลขที่บัตรประชาชน</label>
          </div>
        </div>
        <div class="col-md-6">
          <div class="checkbox">
            <label>
              <input type="checkbox" name="hide_list[]" value="mem_dob" data-smk-msg="&nbsp;">วันเดือนปีเกิด</label>
          </div>
        </div>
        <div class="col-md-6">
          <div class="checkbox">
            <label>
              <input type="checkbox" name="hide_list[]" value="email" data-smk-msg="&nbsp;">Email</label>
          </div>
        </div>
        <div class="col-md-6">
          <div class="checkbox">
            <label>
              <input type="checkbox" name="hide_list[]" value="tel_no" data-smk-msg="&nbsp;">เบอร์โทร</label>
          </div>
        </div>
      </div>
    </div>
    <div class="box-footer">
      <div class="col-md-12" align="center">
          <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
          <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
      </div>
    </div>
  </div>
  <div class="box box-success" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1);">
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div id="showTablePrivate"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(function () {
    $('.select2').select2();
    showTablePrivate();
  })
</script>
