<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$consent_name   = "";
$consent_desc   = "";
$is_active      = "";


$disabled   = "";
$disabled2  = "disabled";
$bg         = "background-color:#fff;";
$display    = "";

if($action == 'ADD')
{
  $disabled2  = "";
}


if($action == 'EDIT' || $action == 'VIEW')
{
  if($action == 'VIEW')
  {
    $disabled = "disabled";
    $bg       = "";
    $display    = "display:none;";
  }

  $sql   = "SELECT * FROM t_consent WHERE consent_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;
  $consent_name   = $row[0]['consent_name'];
  $consent_desc   = $row[0]['consent_desc'];
  $is_active      = $row[0]['is_active'];

}

?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="consent_id" value="<?=$id?>">
<div class="modal-body">
        <div class="row">
            <div class="col-md-9">
              <div class="form-group">
                <label>รายการ</label>
                <input type="text" <?= $disabled ?> value="<?= $consent_name ?>" name="consent_name" class="form-control" placeholder="รายการ" required="">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>สถานะ</label>
                <select name="is_active" <?= $disabled ?> class="form-control " style="width: 100%;" required >
                  <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
                  <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>ข้อความ</label>
                <div id="editor"><?=@$consent_desc?></div>
                <input type="hidden" id="consent_desc" name="consent_desc">
              </div>
            </div>
        </div>
        <!-- /.row -->
    <!-- </div> -->
  <!-- </div> -->
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
    init();
  })
</script>
