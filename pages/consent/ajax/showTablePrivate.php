<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>
<table class="table table-bordered table-striped table-hover" id="tableDisplay2">
  <thead>
    <tr class="text-center">
      <th style="width:50px">ลำดับ</th>
      <th >รายการ</th>
      <th style="width:200px">ปกปิดข้อมูลส่วนบุคคล</th>
      <th style="width:50px">ลบ</th>
    </tr>
  </thead>
  <?php
  $consent_id   = isset($_POST['consent_id'])?$_POST['consent_id']:"";

  $sql  = "SELECT * FROM t_consent_data WHERE consent_id = '$consent_id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];



  if($dataCount > 0)
  {
    foreach ($row as $key => $value) {
      $consent_data_id       = $value['consent_data_id'];
      $consent_data_name     = $value['consent_data_name'];
      $hide_list             = $value['hide_list'];

      $arrHide = explode(",", trim($hide_list));
      $arrData = array("mem_name"=>"ชื่อ","mem_last"=>"นามสกุล","mem_idcard"=>"เลขที่บัตรประชาชน","mem_dob"=>"วันเดือนปีเกิด","email"=>"Email","tel_no"=>"เบอร์โทร");

      $str = "";
      //print_r($arrHide);
      //print_r($arrData);
      foreach ($arrHide as $value)
      {
        if($value != "")
        {
          if($str == "")
          {
            $str = "- ".$arrData[$value];
          }else{
            $str .= "<br>- ".$arrData[$value];
          }
        }
      }
  ?>
      <tr class="text-center">
        <td><?=$key+1;?></td>
        <td align="left"><?=$consent_data_name ?></td>
        <td align="left"><?=$str;?></td>
        <td align="center">
          <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="delPrivate('<?=$consent_data_id?>')"></i></a>
        </td>
      </tr>
  <?php
}}
  ?>
  <tbody>
  </tbody>
</table>

<script>
  $(function () {
    $('#tableDisplay2').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : false,
      'info'        : false,
      'autoWidth'   : false
    });
  })
</script>
