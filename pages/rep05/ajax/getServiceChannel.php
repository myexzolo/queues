<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$point_id       = @$_POST['point_id'];
$agencyCode     = @$_POST['agencyCode'];

$con = "";

if($point_id != "")
{
  	 $con = " and point_id = '$point_id'";
}

  $sqla   = "SELECT *
             FROM t_service_channel
             where is_active not in ('D') and agency_code = '$agencyCode' and point_id = $point_id
             ORDER BY service_channel";

  $querya      = DbQuery($sqla,null);
  $jsona       = json_decode($querya, true);
  $dataCounta  = $jsona['dataCount'];
  $rowA        = $jsona['data'];

   //print_r($rowA);
?>
<select id="serviceChannel" class="form-control select2" style="width: 100%;">
  <?php
    if($dataCounta > 1 || $point_id == "")
    {
      echo '<option value="">ทุกช่องบริการ</option>';
    }
    if($point_id != "")
    {
      for ($s=0; $s < $dataCounta; $s++) {
        $service_channel    = $rowA[$s]['service_channel'];
        $service_channel_id = $rowA[$s]['service_channel_id'];

        echo '<option value="'.$service_channel.'">'.$service_channel.'</option>';
      }
    }
  ?>
</select>
<script>
$(function() {
  $('.select2').select2();
});
</script>
