<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<style>
th{
  vertical-align: middle !important;
}
</style>
<?php
$startDate            = $_POST['startDate'];
$endDate              = $_POST['endDate'];
$typeSearch           = @$_POST['typeSearch'];
$service_id           = @$_POST['service_id'];
$point_id             = @$_POST['point_id'];
$agencyCode           = @$_POST['agencyCode'];

$startDateStr = DatetoThaiMonth($startDate);
$endDateStr   = DatetoThaiMonth($endDate);

$textDate = "";
if($startDate == $endDate){
  $textDate = "วันที่ ".$startDateStr;
}else{
  $textDate = "ตั้งแต่วันที่ ".$startDateStr." - ".$endDateStr;
}

$startDate  = $_POST['startDate']." 00:00:01";
$endDate    = $_POST['endDate']." 23:59:59";

$con = "";

if($typeSearch == "1")
{
  $str1 = "แยกตามจุดบริการ (กลุ่มเคาน์เตอร์)";
  if($point_id != "")
  {
      $con = " and point_id = '$point_id'";
  }
  $sql  = "SELECT * from t_point_service WHERE agency_code = '$agencyCode' and is_active = 'Y' $con ";
}else{
  $str1 = "แยกตามประเภทบริการ";
  if($service_id != "")
  {
      $con = " and service_id = '$service_id' ";
  }
  $sql  = "SELECT * from t_service_agency WHERE agency_code = '$agencyCode' and is_active = 'Y' $con order by cat_service_id, service_id";
}

// $sql = "SELECT q.*,TIMESTAMPDIFF(SECOND,q.date_service,q.date_end) as time_service, a.agency_name, s.kpi_time_a, s.service_name_a
//         FROM t_trans_queue q, t_agency a, t_service_agency s
//         WHERE q.date_start between '$startDate' and '$endDate' and status_queue = 'E'
//         and  q.agency_code = a.agency_code and q.agency_code = s.agency_code and q.service_id = s.service_id $con
//         ORDER BY q.agency_code,q.date_service";

$querys     = DbQuery($sql,null);
$json       = json_decode($querys, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$rows       = $json['data'];

?>
<div class="box-body">
<div align="center">
  <p style="font-size:24px;font-weight:600">รายงานสรุปจำนวนผู้มาติดต่อที่มารับบริการ</p>
  <p style="font-size:24px;font-weight:600"><?= $str1 ?></p>
  <p style="font-size:22px;font-weight:600"><?=$textDate?></p>
</div>
<input type="hidden" id="dataCount" value="<?=$dataCount?>">
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px;">ลำดับที่</th>
      <th >รายการ</th>
      <th >ช่องบริการ</th>
      <th style="width:270px;">จำนวนผู้มาติดต่อที่มารับบริการ (ราย)</th>
      <th style="width:270px;">จำนวนผู้มาติดต่อที่พลาดการรับบริการ (ราย)</th>
      <th style="width:270px;">จำนวนผู้ได้รับบริการ (ราย)</th>
    </tr>
  </thead>
<tbody>
  <?php

      $agencyCodeTmp = "";
      for($i = 0; $i < $dataCount; $i++)
      {
        if($typeSearch == "1")
        {
            $service_id_list = $rows[$i]['service_id_list'];
            $point_id        = $rows[$i]['point_id'];
            $detail          = $rows[$i]['point_detail'];

            $sqls  = "SELECT s.service_channel,
                     SUM(CASE WHEN t.status_queue = 'E' THEN 1 ELSE 0 END) AS service,
                     SUM(CASE WHEN t.status_queue = 'C' THEN 1 ELSE 0 END) AS cencel
                     from t_service_channel s
                     LEFT JOIN t_trans_queue t ON s.service_channel = t.service_channel
                               and t.service_id in ($service_id_list) and t.agency_code = '$agencyCode'
                               and t.status_queue in ('E','C') and t.date_start between '$startDate' and '$endDate'
                     WHERE s.agency_code = '$agencyCode' and s.is_active = 'Y' and s.point_id = '$point_id'
                     group by s.service_channel
                     order by s.service_channel";
        }
        else
        {
          $service_id      = $rows[$i]['service_id'];
          $detail          = $rows[$i]['service_name_a'];

          $sqls  = "SELECT s.service_channel,
                   SUM(CASE WHEN t.status_queue = 'E' THEN 1 ELSE 0 END) AS service,
                   SUM(CASE WHEN t.status_queue = 'C' THEN 1 ELSE 0 END) AS cencel
                   from t_service_channel s
                   LEFT JOIN t_trans_queue t ON s.service_channel = t.service_channel
                             and t.service_id = $service_id and t.agency_code = '$agencyCode'
                             and t.status_queue in ('E','C') and t.date_start between '$startDate' and '$endDate'
                   WHERE s.agency_code = '$agencyCode' and s.is_active = 'Y' and find_in_set('$service_id',s.service_id_list) <> 0
                   group by s.service_channel
                   order by s.service_channel";
          //echo $sqls;
        }

          $numShow = 0;

          $query     = DbQuery($sqls,null);
          $json       = json_decode($query, true);
          $errorInfos  = $json['errorInfo'];
          $dataCounts  = $json['dataCount'];
          $row         = $json['data'];

          for($x = 0; $x < $dataCounts; $x++)
          {
            $numShow++;
            $service_channel = $row[$x]['service_channel'];
            $service = $row[$x]['service'];
            $cencel  = $row[$x]['cencel'];
            $total   = $service - $cencel;

      ?>
        <tr class="text-center">
          <td ><?=$numShow;?></td>
          <td style="text-align:center;"><?= $detail ?></td>
          <td style="text-align:center;"><?= $service_channel ?></td>
          <td style="text-align:right;"><?= $service ?></td>
          <td style="text-align:right;"><?= $cencel ?></td>
          <td style="text-align:right;"><?= $total ?></td>
        </tr>
      <?php
        }
      }
    ?>
  </tbody>
</table>
</div>
<div class="box-footer">
  <button type="button" class="btn btn-success btn-flat pull-right btn-rep" onclick="printPdf()" >พิมพ์</button>
</div>
<script>
  $(function () {
    var groupColumn = 1;
    $('#tableDisplay').DataTable({
      'paging'        : true,
      'lengthChange'  : false,
      'searching'     : false,
      'ordering'      : false,
      'info'          : true,
      'autoWidth'     : false,
      'columnDefs'    : [{ "visible": false, "targets": groupColumn }],
      'order'         : [[ groupColumn, 'asc' ]],
      'drawCallback'  : function ( settings ) {
                        var api = this.api();
                        var rows = api.rows( {page:'current'} ).nodes();
                        var last=null;

                        api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                          if ( last !== group )
                          {
                            $(rows).eq( i ).before(
                                '<tr class="group"><td colspan="6"><b>'+group+'</b></td></tr>'
                            );
                            last = group;
                          }
                      } );
                      }
    })
  })
</script>
