<!DOCTYPE html>
  <?php
  $pageName     = "รายงานสรุปจำนวนผู้มาติดต่อที่มารับบริการ";
  $pageCode     = "REP05";
  ?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>ระบบสนับสุนนการให้บริการ (Queue) - <?= $pageName ?></title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];

        $agencyCode = $_SESSION['AGENCY_CODE'];
      ?>
      <link rel="stylesheet" href="css/rep05.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php include("../../inc/sidebar.php"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              <?= $pageName ?>
              <small><?=$pageCode ?></small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="../../pages/home/"><i class="fa fa-home"></i> หน้าหลัก</a></li>
              <li class="active"><?= $pageName ?></li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php"); ?>
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
              <div class="col-md-12">
                <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">ค้นหารายงาน</h3>
                    <input type="hidden" value="<?= $agencyCode ?>" id="agencyCode">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>เลือกวันที่</label>
                          <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width:250px;">
                              <i class="fa fa-calendar"></i>&nbsp;
                              <span></span> <i class="fa fa-caret-down pull-right" style="line-height: 29px;"></i>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>รูปแบบการค้นหา</label>
                          <select id="typeSearch" class="form-control select2" style="width: 100%;" onchange="getTypeSearch()">
                            <option value="1">จุดบริการ (กลุ่มเคาน์เตอร์)</option>
                            <option value="2">ประเภทบริการ</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3" id="div_point" >
                        <div class="form-group">
                          <label>จุดบริการ (กลุ่มเคาน์เตอร์)</label>
                        <?php
                          $sqla   = "SELECT *
                                     FROM t_point_service
                                     where is_active not in ('D') and agency_code = '$agencyCode'
                                     ORDER BY point_number";

                          $querya      = DbQuery($sqla,null);
                          $jsona       = json_decode($querya, true);
                          $dataCounta  = $jsona['dataCount'];
                          $rowA        = $jsona['data'];

                           //print_r($rowA);
                        ?>
                        <select id="pointService" class="form-control select2" style="width: 100%;">
                          <?php
                            echo '<option value="">ทุกจุดบริการ</option>';
                            for ($s=0; $s < $dataCounta; $s++) {
                              $point_detail   = $rowA[$s]['point_detail'];
                              $point_id       = $rowA[$s]['point_id'];

                              echo '<option value="'.$point_id.'">'.$point_detail.'</option>';
                            }
                          ?>
                        </select>
                        </div>
                      </div>
                      <div class="col-md-4" id="div_service" style="display:none">
                        <div class="form-group">
                          <label>ประเภทบริการ</label>
                          <?php
                            $sqla   = "SELECT s.* ,c.cat_service_name
                                       FROM t_service_agency s, t_cat_service c
                                       where c.is_active not in ('D') and s.cat_service_id = c.cat_service_id and s.agency_code = '$agencyCode'
                                       ORDER BY c.cat_service_id, s.service_id";
                            //echo  $sqla;
                            $querya      = DbQuery($sqla,null);
                            $jsona       = json_decode($querya, true);
                            $dataCounta  = $jsona['dataCount'];
                            $rowA        = $jsona['data'];

                          ?>
                          <select id="catService" class="form-control select2" style="width: 100%;">
                          <?php
                            if($dataCounta > 1)
                            {
                              echo '<option value="">ทุกงานบริการ</option>';
                            }
                            for ($s=0; $s < $dataCounta; $s++) {
                              $service_name_a  = $rowA[$s]['service_name_a'];
                              $service_id    = $rowA[$s]['service_id'];

                              echo '<option value="'.$service_id.'">'.$service_name_a.'</option>';
                            }
                          ?>
                          </select>
                        </div>
                      </div>
                   </div>
                </div>
                <div class="box-footer with-border" align="center">
                    <input type="hidden" id="startDate">
                    <input type="hidden" id="endDate">
                    <button class="btn btn-primary btn-flat btn-rep" onclick="showTable()" >ค้นหา</button>
                </div>
              </div>
              </div>

              <div class="col-md-12">
                <div class="box box-primary" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1);">
                  <div class="box-header with-border">
                      <button type="button" class="btn btn-success btn-flat pull-right btn-rep" onclick="printPdf()" >พิมพ์</button>
                  </div>
                  <!-- /.box-header -->
                  <div id="showTable"></div>
                </div>
              </div>
            </div>
            <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/rep05.js"></script>
    </body>
  </html>
