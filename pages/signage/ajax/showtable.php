<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/connectSignex.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:60px">ลำดับ</th>
      <th>ชื่อ Player</th>
      <th>Platform</th>
      <th>Date Active</th>
    </tr>
  </thead>
  <tbody>
    <?php

      $sql  = "SELECT * FROM player where id in (6,8,9,11,12,13) ORDER BY name";

      $query      = DbQuerySignex($sql,null);
      $json       = json_decode($query, true);
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      for($x = 0; $x < $dataCount; $x++)
      {
        $new_date = date("Y-m-d H:i:s", strtotime($rows[$x]['last_active'] . " +7 hours"));
    ?>
    <tr class="text-center">
      <td><?=$x+1;?></td>
      <td align="center"><?=$rows[$x]['name']?></td>
      <td align="center"><?=$rows[$x]['platform']?></td>
      <td align="center"><?=DateTimeThai($new_date); ?></td>
    </tr>
  <?php } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });
  })
</script>
