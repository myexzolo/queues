<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action         = isset($_POST['action'])?$_POST['action']:"";
$question_id    = isset($_POST['question_id'])?$_POST['question_id']:"";


//print_r($_POST);
unset($_POST["action"]);

$arrData['question_code']   = $_POST['question_code'];
$arrData['question_detail'] = $_POST['question_detail'];
$arrData['max_score']       = $_POST['max_score'];
$arrData['question_type']   = $_POST['question_type'];
$arrData['is_active']       = $_POST['is_active'];


if($action == "ADD")
{
    unset($_POST["question_id"]);
    $_POST["date_create"] = date('Y/m/d H:i:s');
    $sql = DBInsertPOST($arrData,'b_questionnaire');
}
else if($action == "EDIT")
{
    $arrData['question_id']   = $question_id;
    $sql = DBUpdatePOST($arrData,'b_questionnaire','question_id');
}
else if($action == "DEL")
{
    $sql = "UPDATE b_questionnaire SET is_active = 'D' WHERE question_id = '$question_id'";
}

// echo $sql;
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($row['errorInfo'][0]) == 0)
{
  if($action == "ADD")
  {
    $question_id = $row['id'];
  }

  $sql = "DELETE FROM b_answer WHERE question_id = '$question_id'";
  DbQuery($sql,null);

  // print_r($_POST);
  if(isset($_POST["answer_desc"]))
  {
    for($x=0; $x < count($_POST["answer_desc"]); $x++)
    {
      $arrAnswer['question_id'] = $question_id;
      $arrAnswer['answer_desc'] = $_POST["answer_desc"][$x];
      $arrAnswer['score'] = $_POST["score"][$x];

      $sql = DBInsertPOST($arrAnswer,'b_answer');
      DbQuery($sql,null);
    }
  }

  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}



?>
