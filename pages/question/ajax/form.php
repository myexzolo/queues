<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";



$question_code          = "";
$question_detail        = "";
$max_score              = "";
$question_type          = "";
$is_active              = "";

$disabled   = "";
$disabled2  = "disabled";
$bg         = "background-color:#fff;";
$display    = "";

if($action == 'ADD')
{
  $disabled2  = "";
  $question_type = "S";
}


if($action == 'EDIT' || $action == 'VIEW')
{
  if($action == 'VIEW')
  {
    $disabled = "disabled";
    $bg       = "";
    $display    = "display:none;";
  }

  // $sql   = "SELECT s.*
  //           FROM t_service s WHERE service_id = '$id'";


  $sql   = "SELECT *
            FROM  b_questionnaire
            where is_active not in ('D') and  question_id = '$id'";


  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;
  $question_code          = $row[0]['question_code'];
  $question_detail        = $row[0]['question_detail'];
  $max_score              = $row[0]['max_score'];
  $question_type          = $row[0]['question_type'];
  $is_active              = $row[0]['is_active'];

}

?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="question_id" value="<?=$id?>">
<div class="modal-body">
    <div class="row">
    <div class="col-md-2">
      <div class="form-group">
        <label>รหัส</label>
        <input type="text" <?= $disabled ?> maxlength="4" value="<?= $question_code ?>" name="question_code" class="form-control"  required>
      </div>
    </div>
    <div class="col-md-10">
      <div class="form-group">
        <label>คำถาม</label>
        <input type="text" <?= $disabled ?> value="<?= $question_detail ?>" name="question_detail" class="form-control" placeholder="" required>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>คะแนนเต็ม</label>
        <input type="text" <?= $disabled ?>  value="<?= $max_score ?>" name="max_score" class="form-control" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>ประเภท</label>
        <div class="radio">
            <label>
              <input type="radio" <?= $disabled ?> name="question_type" value="S" <?=@$question_type=='S'?"checked":""?> required onchange="showTableAnswer('<?=$id?>','<?=$action?>')">
              แสดงคำถาม
            </label>
            &nbsp;&nbsp;&nbsp;
            <label>
              <input type="radio" <?= $disabled ?> name="question_type" value="E" <?=@$question_type=='E'?"checked":""?> required onchange="showTableAnswer('<?=$id?>','<?=$action?>')">
              แสดงข้อความ
            </label>
        </div>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>สถานะ</label>
        <select name="is_active" <?= $disabled ?> class="form-control " style="width: 100%;" required >
          <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
          <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
        </select>
      </div>
    </div>

    <div class="col-md-12">
      <div id="showTableAnswer"></div>
    </div>
</div>
        <!-- /.row -->
    <!-- </div> -->
  <!-- </div> -->
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
    showTableAnswer('<?=$id?>','<?=$action?>');
  })
</script>
