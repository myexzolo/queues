<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action         = $_POST['action'];
$question_id    = isset($_POST['question_id'])?$_POST['question_id']:"";

$disabled   = "";

if($action == 'VIEW')
{
  $disabled = "disabled";
}

?>
<div class="box box-primary" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1);">
  <div class="box-header with-border">
    <h3 class="box-title"></h3>
    <div class="pull-right">
      <div class="rows">
        <div style="float: right;">
          <a class="btn btn-social btn-success btnAdd pull-right btn-flat" onclick="addAnswer()">
            <i class="fa fa-plus"></i> คำตอบ
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="box-body">
    <table class="table table-bordered table-striped table-hover" id="tableDisplay2">
      <thead>
        <tr class="text-center">
          <th style="width:50px">ลำดับ</th>
          <th >คำตอบ</th>
          <th style="width:80px" >คะแนน</th>
          <th style="width:50px;">ลบ</th>
        </tr>
      </thead>
      <tbody>
        <?php


        $sqls   = "SELECT *
                   FROM b_answer
                   where question_id  = '$question_id'
                   ORDER BY score desc";

        //echo $sqls;
        $querys     = DbQuery($sqls,null);
        $json       = json_decode($querys, true);
        $errorInfo  = $json['errorInfo'];
        $dataCount  = $json['dataCount'];
        $rows       = $json['data'];

        if($dataCount > 0)
        {
          foreach ($rows as $key => $value)
          {
            $id_tr = "tr_".$value['question_id'];
        ?>
        <tr class="text-center" id="<?= $id_tr ?>">
          <td><?=$key+1;?></td>
          <td align="left">
            <div class="form-group" style="width: 100%;margin-bottom: 0px;">
            <input type="text" <?= $disabled ?> value="<?=$value['answer_desc']?>" name="answer_desc[]" class="form-control" style="width: 100%;" required>
          </div>
          </td>
          <td align="center">
            <div class="form-group" style="width: 100%;margin-bottom: 0px;">
            <input type="text" <?= $disabled ?>  value="<?=$value['score']?>" name="score[]" class="form-control" required style="width: 100%;">
          </div>
          </td>
          <td align="center">
            <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="delTr('<?=$id_tr ?>')"></i></a>
          </td>
        </tr>
        <?php
          }
        }else{
        ?>
        <tr class="text-center">
          <td>1</td>
          <td>
            <div class="form-group" style="width: 100%;margin-bottom: 0px;">
              <input type="text" <?= $disabled ?> value="" name="answer_desc[]" class="form-control" placeholder="" required style="width: 100%;">
            </div>
          </td>
          <td>
            <div class="form-group" style="width: 100%;margin-bottom: 0px;">
              <input type="text" <?= $disabled ?>  value="" name="score[]" class="form-control" required style="width: 100%;">
            </div>
          </td>
          <td></td>
        </tr>
      <?php } ?>
      </tbody>
    </table>
  </div>
</div>

<script>
  $(function () {
    $('.select2').select2();

    $('#tableDisplay2').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : false,
      'info'        : false,
      'autoWidth'   : false
    });
  })
</script>
