$(function () {
  pushMenu();
  showTable();
})


function pushMenu()
{
  if($( window ).width() < 1900){
     $('[data-toggle="push-menu"]').pushMenu('toggle');
  }
}


function showTable(){
  $.get("ajax/showTable.php")
    .done(function( data ) {
      $('#showTable').html(data);
  });
}

function showTableAnswer(question_id,action){
  var question_type  = $("input[name='question_type']:checked").val();
  //console.log(question_type);
  if(question_type == 'E'){
    $('#showTableAnswer').html("");
  }else
  {
    $.post("ajax/showTableAnswer.php",{question_id:question_id,action:action})
      .done(function( data ) {
        $('#showTableAnswer').html(data);
    });
  }

}

function delTr(id)
{
  $('#'+id).remove();
}


function addAnswer()
{
  var currentdate = new Date();

  var idTr = "tr_" + currentdate.getSeconds() + currentdate.getMilliseconds();
  var str = "<tr id=\""+idTr+"\">"
            +"<td></td>"
            +"<td>"
            +"  <div class=\"form-group\" style=\"width: 100%;margin-bottom: 0px;\">"
            +"  <input type=\"text\" name=\"answer_desc[]\" class=\"form-control\" placeholder=\"\" required style=\"width: 100%;\">"
            +"</div>"
            +"</td>"
            +"<td>"
            +"<div class=\"form-group\" style=\"width: 100%;margin-bottom: 0px;\">"
            +"<input type=\"text\" name=\"score[]\" class=\"form-control\" required style=\"width: 100%;\">"
            +"</div>"
            +"</td>"
            +"<td align=\"center\">"
            +"<a class=\"btn_point text-red\"><i class=\"fa fa-trash-o\" onclick=\"delTr('"+idTr+"')\"></i></a>"
            +"</td>"
            +"</tr>";

   $('#tableDisplay2').append(str);
}





function showForm(action,id){
  $.post("ajax/form.php",{action:action,id:id})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}



function del(id,name){
  $.smkConfirm({
    text:'ยืนยันการลบข้อมูล คำถาม : '+ name +' ?',
    accept:'Yes',
    cancel:'No'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/AED.php",{action:'DEL',agency_id:id})
        .done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#fff',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

function checknumber(inputs){
  var valid = /^\d{0,10}(\.\d{0,2})?$/.test(inputs.value),
      val = inputs.value;
  if(!valid){
      inputs.value = val.substring(0, val.length - 1);
  }
}

function chkNumber(ele)
{
    var vchar = String.fromCharCode(event.keyCode);
    if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
    ele.onKeyPress=vchar;
}


$('#formAED').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAED').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAED').smkClear();
        showTable();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});
