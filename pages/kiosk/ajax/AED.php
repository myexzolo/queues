<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action            = isset($_POST['action'])?$_POST['action']:"";
$kiosk_id          = isset($_POST['kiosk_id'])?$_POST['kiosk_id']:"";
$menu_id_list      = isset($_POST['menu_id_list'])?$_POST['menu_id_list']:"";

unset($_POST["action"]);

if($menu_id_list != "")
{
  $_POST["menu_id_list"] = implode(",",$menu_id_list);
}
$_POST["status_send"] = "N"; //เพิ่ม



if($action == "ADD")
{
    unset($_POST["kiosk_id"]);
    $_POST["date_create"] = date('Y/m/d H:i:s');
    $sql = DBInsertPOST($_POST,'t_kiosk');
}
else if($action == "EDIT")
{
    $sql = DBUpdatePOST($_POST,'t_kiosk','kiosk_id');
}
else if($action == "DEL")
{
    $sql = "UPDATE t_kiosk SET is_active = 'D', status_send = 'N' WHERE kiosk_id = '$kiosk_id'";
}

// echo $sql;

$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($row['errorInfo'][0]) == 0){

    if($action == "ADD")
    {
      $kiosk_id  = $row['id']; ///แก้ ID
    }


    $sqls   = "SELECT m.* , p.ref_code as point_ref_code
               FROM t_kiosk m, t_point_service p
               where kiosk_id = '$kiosk_id' and m.point_id = p.point_id"; //เปลี่ยน table

    // echo $sqls;

    $querys     = DbQuery($sqls,null);
    $json       = json_decode($querys, true);
    $rows       = $json['data'][0];

    $rows["action"]   = "U";
    $agency_code = $rows["agency_code"];

    if($action == 'ADD')
    {
      $rows["ref_code"] = $agency_code."#C".$kiosk_id;   ///แก้ ID
      $rows["action"]   = "ADD";
    }

    $rows        = chkDataEmpty($rows);


    $ipAgency    = getIPbyAgency($agency_code);
    // $ipAgency    = $_SESSION['IP_CENTER'];  //เปลี่ยน  ipAgency

    $data_array  = array(
                       "functionName" => "manageKiosk",  ///แก้ ชื่อ Service
                       "dataJson" => $rows,
                     );

    $url        = "http://$ipAgency/ws/service.php";

    // echo $url;
    // echo json_encode($data_array);

    $make_call = callAPI('POST', $url, json_encode($data_array));
    $response = json_decode($make_call, true);
    $status   = $response['status'];
    $data     = $response['data'];

    $arrUpdate['kiosk_id']        =  $kiosk_id; //แก้ ID
    $arrUpdate['ref_code']        =  $rows["ref_code"];
    $arrUpdate['status_send']     =  "N";

    //print_r($response);
    $sql = "";

    if($status == "200")
    {
        $arrUpdate['status_send']  =  "S";
    }else{
      $arrLog['url']  = $url;
      $arrLog['data'] = json_encode($data_array,JSON_UNESCAPED_UNICODE);
      $arrLog['table_name']   = 't_kiosk';   // เปลี่ยน Table
      $arrLog['id_update']    = 'kiosk_id';// เปลี่ยน ID
      $arrLog['date_create']  = date('Y/m/d H:i:s');
      $arrLog['data_update']  = json_encode($arrUpdate);

      $sql = DBInsertPOST($arrLog,'t_log_send_service'); ///แก้ ชื่อ table
    }

    $sql .= DBUpdatePOST($arrUpdate,'t_kiosk','kiosk_id'); ///แก้ ชื่อ table
    DbQuery($sql,null);

    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}



?>
