<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$point_id          = isset($_POST['id'])?$_POST['id']:"";
$menu_id_list      = isset($_POST['menu_id_list'])?$_POST['menu_id_list']:"";

$agencyCode = $_SESSION['AGENCY_CODE'];


$menuIdList = array();
if($menu_id_list != "")
{
  $menuIdList = explode(",",$menu_id_list);
}

$sqls   = "SELECT *
           FROM t_menu
           where is_active = 'Y' and agency_code = '$agencyCode' and point_id = $point_id and ref_code is not null
           ORDER BY start_time";

//echo $sqls;

$querys     = DbQuery($sqls,null);
$json       = json_decode($querys, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$rows       = $json['data'];


if($dataCount > 0)
{
  foreach ($rows as $key => $value)
  {
    $ref_code  = $value['ref_code'];
    $menu_name = $value['menu_name'];
    $type_menu = $value['type_menu'];

    $checked = "";
    if(in_array($ref_code, $menuIdList))
    {
      $checked = "checked";
    }
    if($type_menu == "2")
    {
      $menu_name = "พัก";
    }
    ?>
        <div class="col-md-12">
          <div class="checkbox">
              <label>
                <input type="checkbox" name="menu_id_list[]"  value="<?=$ref_code ?>" <?=$checked ?> >
                <?=$menu_name." (".$value['start_time']."-".$value['end_time'].")"?>
              </label>
          </div>
        </div>
    <?php
  }
}
?>
