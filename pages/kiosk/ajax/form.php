<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$agencyCode = $_SESSION['AGENCY_CODE'];

$kiosk_code         = "";
$kiosk_name         = "";
$menu_id_list       = "";
$service_open       = "";
$service_close      = "";
$kiosk_open         = "";
$kiosk_close        = "";
$is_active          = "";
$point_id           = "";
$ip                 = "";
$text_alert         = "";


$disabled   = "";
$disabled2  = "disabled";
$bg         = "background-color:#fff;";
$display    = "";

$ldt = "checked";
$lde = "";

if($action == 'ADD')
{
  $disabled2  = "";
}


if($action == 'EDIT' || $action == 'VIEW')
{
  if($action == 'VIEW')
  {
    $disabled = "disabled";
    $bg       = "";
    $display    = "display:none;";
  }

  $sql   = "SELECT * FROM t_kiosk WHERE kiosk_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;
  $kiosk_code         = $row[0]['kiosk_code'];
  $kiosk_name         = $row[0]['kiosk_name'];
  $menu_id_list       = !empty(@$row[0]['menu_id_list'])?$row[0]['menu_id_list']:"";
  $service_open       = $row[0]['service_open'];
  $service_close      = $row[0]['service_close'];
  $kiosk_open         = $row[0]['kiosk_open'];
  $kiosk_close        = $row[0]['kiosk_close'];
  $point_id           = $row[0]['point_id'];
  $is_active          = $row[0]['is_active'];
  $ip                 = $row[0]['ip'];
  $text_alert         = $row[0]['text_alert'];

}
  $optionPoint = getoptionPoint($point_id,$agencyCode);
?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" id="menu_id_list" value="<?=$menu_id_list?>">
<input type="hidden" name="kiosk_id"   value="<?=$id?>">
<input type="hidden" name="agency_code" value="<?=$agencyCode?>">
<div class="modal-body">
  <div class="row">
      <div class="col-md-2">
        <div class="form-group">
          <label>จุดบริการที่</label>
          <select name="point_id" <?= $disabled ?> id="point_id" class="form-control select2" style="width: 100%;" required onchange="listMenu()">
            <option value="">&nbsp;</option>
            <?= $optionPoint ?>
          </select>
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label>Kiosk Code</label>
          <input type="text" <?= $disabled ?> value="<?= $kiosk_code ?>" name="kiosk_code" class="form-control"  placeholder="" required>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Kiosk Name</label>
          <input type="text" <?= $disabled ?> value="<?= $kiosk_name ?>" name="kiosk_name" class="form-control"  placeholder="" required>
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label>สถานะ</label>
          <select name="is_active" <?= $disabled ?> class="form-control " style="width: 100%;" required >
            <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
            <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
          </select>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>IP Kiosk</label>
          <input type="text" <?= $disabled ?> value="<?= $ip ?>" name="ip" class="form-control"  placeholder="" required>
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label>เวลาเปิดเครื่อง</label>
          <input type="text" <?= $disabled ?> value="<?= $kiosk_open ?>" name="kiosk_open" class="form-control" placeholder="00:00" required>
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label>เวลาปิดเครื่อง</label>
          <input type="text" <?= $disabled ?> value="<?= $kiosk_close ?>" name="kiosk_close" class="form-control" placeholder="00:00" required>
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label>เวลาเปิดบริการ</label>
          <input type="text" <?= $disabled ?> value="<?= $service_open ?>" name="service_open" class="form-control" placeholder="00:00" required>
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label>เวลาปิดบริการ</label>
          <input type="text" <?= $disabled ?> value="<?= $service_close ?>" name="service_close" class="form-control" placeholder="00:00" required>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label>ข้อความแจ้งเตือน</label>
          <input type="text" <?= $disabled ?> value="<?= $text_alert ?>" name="text_alert" class="form-control" >
        </div>
      </div>

    </div>
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">เมนู</h3>
        </div>
        <div class="box-body">
          <div id="listMenu"></div>
        </div>

      <!-- /.box-body -->
    </div>
        <!-- /.row -->
    <!-- </div> -->
  <!-- </div> -->
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
    listMenu();
  })
</script>
