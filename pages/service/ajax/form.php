<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";


$service_lng      = "";
$service_name     = "";
$service_name_en  = "";
$service_code     = "";
$cat_service_id   = "";
$kpi_time         = "";
$waiting_time     = "";
$max_service      = "";
$num_reserve      = "";
$is_active        = "";
$check_list       = "";
$transfer_service_id       = "";

$disabled   = "";
$disabled2  = "disabled";
$bg         = "background-color:#fff;";
$display    = "";

if($action == 'ADD')
{
  $disabled2  = "";
}


if($action == 'EDIT' || $action == 'VIEW')
{
  if($action == 'VIEW')
  {
    $disabled = "disabled";
    $bg       = "";
    $display    = "display:none;";
  }

  $sql   = "SELECT * FROM t_service WHERE service_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;
  $service_name     = $row[0]['service_name'];
  $service_name_en  = $row[0]['service_name_en'];
  $service_code     = $row[0]['service_code'];
  $cat_service_id   = $row[0]['cat_service_id'];
  $kpi_time         = $row[0]['kpi_time'];
  $waiting_time     = $row[0]['waiting_time'];
  $max_service      = $row[0]['max_service'];
  $num_reserve      = $row[0]['num_reserve'];
  $is_active        = $row[0]['is_active'];
  $check_list       = $row[0]['check_list'];
  $reserve_status   = $row[0]['reserve_status'];
  // $transfer_service_id  = $row[0]['transfer_service_id'];
}

  $optionCatService = getoptionCatService($cat_service_id);
?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="service_id" value="<?=$id?>">
<div class="modal-body">
        <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label>รหัสงานบริการ</label>
                <input type="text" <?= $disabled ?> maxlength="2" value="<?= $service_code ?>" name="service_code" class="form-control"  required>
              </div>
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <label>ชื่องานบริการ TH</label>
                <input type="text" <?= $disabled ?> value="<?= $service_name ?>" name="service_name" class="form-control" placeholder="ชื่องานบริการ ภาษาไทย" required>
              </div>
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <label>ชื่องานบริการ EN</label>
                <input type="text" <?= $disabled ?> value="<?= $service_name_en ?>" name="service_name_en" class="form-control" placeholder="ชื่องานบริการ ภาษาอังกฤษ" >
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>เวลามาตราฐาน</label>
                <input type="text" <?= $disabled ?> OnKeyPress="return chkNumber(this)" value="<?= $kpi_time ?>" name="kpi_time" class="form-control" required>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>เวลารอคอย</label>
                <input type="text" <?= $disabled ?> OnKeyPress="return chkNumber(this)" value="<?= $waiting_time ?>" name="waiting_time" class="form-control" required>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>จำนวนผู้รับบริการ</label>
                <input type="text" <?= $disabled ?> OnKeyPress="return chkNumber(this)" value="<?= $max_service ?>" name="max_service" class="form-control" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>หมวดงานบริการ</label>
                <select name="cat_service_id" <?= $disabled ?> class="form-control select2" style="width: 100%;" required>
                  <option value="">หมวดงาน</option>
                  <?= $optionCatService ?>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>สถานะจอง Online</label>
                <select name="reserve_status" <?= $disabled ?> class="form-control " style="width: 100%;" required >
                  <option value="N" <?=@$reserve_status=='N'?"selected":""?>>ไม่เปิดจอง</option>
                  <option value="Y" <?=@$reserve_status=='Y'?"selected":""?>>เปิดจอง</option>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>จำนวนรับจองคิว</label>
                <input type="text" <?= $disabled ?> OnKeyPress="return chkNumber(this)" value="<?= $num_reserve ?>" name="num_reserve" class="form-control">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>สถานะ</label>
                <select name="is_active" <?= $disabled ?> class="form-control " style="width: 100%;" required >
                  <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
                  <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
                </select>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>รายการเอกสารที่ต้องใช้ (Check list)</label>
                <div id="editor"><?=@$check_list?></div>
                <input type="hidden" id="check_list" name="check_list">
              </div>
            </div>
        </div>
        <!-- /.row -->
    <!-- </div> -->
  <!-- </div> -->
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
    init();
  })
</script>
