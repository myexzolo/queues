<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px">ลำดับ</th>
      <th>จุดบริการ</th>
      <th>ช่องบริการ</th>
      <th style="width:120px">รับงานอัตโนมัติ</th>
      <th style="width:90px">สถานะ</th>
      <th style="width:50px;">ดู</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:50px;">แก้ไข</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <th style="width:50px;">ลบ</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $agencyCode = $_SESSION['AGENCY_CODE'];
      $point_id   = $_POST['point_id'];
      $con        = "";
      if($point_id != "")
      {
          $con = " and p.point_id = '$point_id' ";
      }

      $sqls   = "SELECT c.*, p.point_number
                 FROM t_service_channel c ,  t_point_service p
                 where c.is_active not in ('D') and p.point_id = c.point_id
                 and c.agency_code = '$agencyCode' and p.agency_code = '$agencyCode' $con
                 ORDER BY point_id , service_channel";

      //echo $sqls;
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      if($dataCount > 0)
      {
        foreach ($rows as $key => $value) {
          $is_active        = $value['is_active'];
          $is_auto_service  = $value['is_auto_service'];
          // $mem_username     = $value['mem_username']?;

          $activeTxt  = "";
          $bg         = "";
          $onclick    = "";
          $disabled   = "";

          $activeTxtR  = "";
          $bgR         = "";



          if($is_active == "Y")
          {
            $activeTxt = "ใช้งาน";
            $bg        = "bg-green-active";
          }else if($is_active == "N"){
            $activeTxt = "ไม่ใช้งาน";
            $bg        = "bg-gray";
          }

          if($is_auto_service == "Y")
          {
            $autoTxt  = "่รับอัตโนมัติ";
            $bgAuto   = "bg-green-active";
          }else if($is_auto_service == "N"){

            $autoTxt  = "ไม่รับอัตโนมัติ";
            $bgAuto   = "bg-gray";
          }

          // if($reserve_status == "Y")
          // {
          //   $activeTxtR = "เปิดจอง";
          //   $bgR        = "bg-green-active";
          // }else if($reserve_status == "N"){
          //   $activeTxtR = "ไม่เปิดจอง";
          //   $bgR        = "bg-gray";
          // }

    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="center"><?=$value['point_number']?></td>
      <td align="center"><?=$value['service_channel']?></td>
      <td align="center"><div class="<?=$bgAuto?>"><?=$autoTxt ?></div></td>
      <td><div class="<?=$bg?>"><?=$activeTxt ?></div></td>
      <td>
        <a class="btn_point text-green"><i class="fa fa-eye" onclick="showForm('VIEW','<?=$value['service_channel_id']?>')"></i></a>
      </td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['service_channel_id']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="del('<?=$value['service_channel_id']?>','<?=$value['service_channel']?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php }} ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });
  })
</script>
