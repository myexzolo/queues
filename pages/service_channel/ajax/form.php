<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$agencyCode = $_SESSION['AGENCY_CODE'];

$service_channel    = "";
$service_id_list    = "";
$is_auto_service    = "";
$point_id           = "";
$is_active          = "";


$disabled   = "";
$disabled2  = "disabled";
$bg         = "background-color:#fff;";
$display    = "";

$ldt = "checked";
$lde = "";

if($action == 'ADD')
{
  $disabled2  = "";
}


if($action == 'EDIT' || $action == 'VIEW')
{
  if($action == 'VIEW')
  {
    $disabled = "disabled";
    $bg       = "";
    $display    = "display:none;";
  }

  $sql   = "SELECT * FROM t_service_channel WHERE service_channel_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;
  $service_channel    = $row[0]['service_channel'];
  $service_id_list    = !empty(@$row[0]['service_id_list'])?$row[0]['service_id_list']:"";
  $point_id           = $row[0]['point_id'];
  $is_active          = $row[0]['is_active'];
  $is_auto_service    = $row[0]['is_auto_service'];

}
  $optionPoint = getoptionPoint($point_id,$agencyCode);
?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="service_id_list" id="service_id_list" value="<?=$service_id_list?>">
<input type="hidden" name="service_channel_id" value="<?=$id?>">
<input type="hidden" name="agency_code" value="<?=$agencyCode?>">
<div class="modal-body">
  <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label>จุดบริการที่</label>
          <select name="point_id" <?= $disabled ?> id="point_id" class="form-control select2" style="width: 100%;" required onchange="listService()">
            <option value="">&nbsp;</option>
            <?= $optionPoint ?>
          </select>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>ช่องบริการ</label>
          <input type="text" <?= $disabled ?> OnKeyPress="return chkNumber(this)" value="<?= $service_channel ?>" name="service_channel" class="form-control"  placeholder="" required>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>รับงานอัตโนมัติ</label>
          <div class="radio">
            <label>
              <input type="radio" name="is_auto_service" value="Y" <?=@$is_auto_service=='Y'?"checked":""?> > รับอัตโนมัติ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </label>
            <label>
              <input type="radio" name="is_auto_service" value="N" <?=@$is_auto_service=='N'?"checked":""?> > ไม่รับอัตโนมัติ
            </label>
          </div>
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label>สถานะ</label>
          <select name="is_active" <?= $disabled ?> class="form-control " style="width: 100%;" required >
            <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
            <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
          </select>
        </div>
      </div>
    </div>
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">งานบริการ</h3>
        </div>
        <div class="box-body">
          <div id="listService"></div>
        </div>

      <!-- /.box-body -->
    </div>
        <!-- /.row -->
    <!-- </div> -->
  <!-- </div> -->
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
    listService();
  })
</script>
