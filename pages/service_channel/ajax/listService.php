<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$point_id             = isset($_POST['id'])?$_POST['id']:"";
$service_id_list      = isset($_POST['service_id_list'])?$_POST['service_id_list']:"";

$agencyCode = $_SESSION['AGENCY_CODE'];


$sqls   = "SELECT *
           FROM t_point_service
           where is_active = 'Y' AND point_id = '$point_id' and agency_code = '$agencyCode'";

$querys         = DbQuery($sqls,null);
$json           = json_decode($querys, true);
$serviceIdList  = isset($json['data'][0]['service_id_list'])?$json['data'][0]['service_id_list']:"";


$servicelist = array();
if($service_id_list != "")
{
  $servicelist = explode(",",$service_id_list);
}

$sqls   = "SELECT *
           FROM t_service_agency
           where is_active = 'Y' AND service_id in ($serviceIdList) and agency_code = '$agencyCode'
           ORDER BY cat_service_id , service_code_a +'' ";

//echo $sqls;

$querys     = DbQuery($sqls,null);
$json       = json_decode($querys, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$rows       = $json['data'];


if($dataCount > 0)
{
  foreach ($rows as $key => $value)
  {
    $service_id = $value['service_id'];

    $checked = "";
    if(in_array($service_id, $servicelist))
    {
      $checked = "checked";
    }
        ?>
        <div class="col-md-6">
          <div class="checkbox">
              <label>
                <input type="checkbox" name="service_id_list[]"  value="<?=$service_id ?>" <?=$checked ?> >
                <?=$value['service_name_a']?>
              </label>
          </div>
        </div>
    <?php
  }
}
?>
