$(function () {
  pushMenu();
  showTable();


})


function chkService(type)
{
  if(type == 1)
  {
    $(".service").prop("disabled", false);
  }else
  {
    $(".service").prop("disabled", true);
  }

}


function pushMenu()
{
  if($( window ).width() < 1900){
     $('[data-toggle="push-menu"]').pushMenu('toggle');
  }
}


function showTable(){
  var pointId = $('#pointId').val();
  $.post("ajax/showTable.php",{point_id:pointId})
    .done(function( data ) {
      $('#showTable').html(data);
  });
}

function listService()
{
  var id = $('#point_id').val();
  var service_id_list = $('#service_id_list').val();

  $.post("ajax/listService.php",{id:id,service_id_list:service_id_list})
    .done(function( data ) {
      $('#listService').html(data);
  });
}

function showForm(action,id){
  $.post("ajax/form.php",{action:action,id:id})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}



function del(id,name){
  $.smkConfirm({
    text:'ยืนยันการลบข้อมูล เมนู : '+ name +' ?',
    accept:'Yes',
    cancel:'No'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/AED.php",{action:'DEL',menu_id:id})
        .done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#fff',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

function checknumber(inputs){
  var valid = /^\d{0,10}(\.\d{0,2})?$/.test(inputs.value),
      val = inputs.value;
  if(!valid){
      inputs.value = val.substring(0, val.length - 1);
  }
}

function chkNumber(ele)
{
    var vchar = String.fromCharCode(event.keyCode);
    if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
    ele.onKeyPress=vchar;
}


$('#formAED').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAED').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAED').smkClear();
        showTable();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});
