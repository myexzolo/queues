<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:60px">ลำดับ</th>
      <th style="width:90px">จุดบริการ</th>
      <th style="width:90px">รหัสบริการ</th>
      <th>ชื่อเมนู</th>
      <th style="width:150px">เวลาแสดง</th>
      <th style="width:120px">จำนวนรับจองคิวต่อวัน</th>
      <th style="width:120px">สถานะ</th>
      <th style="width:60px;">ดู</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:60px;">แก้ไข</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <th style="width:60px;">ลบ</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $agencyCode = $_SESSION['AGENCY_CODE'];

      $point_id   = $_POST['point_id'];
      $con        = "";
      if($point_id != "")
      {
          $con = " and p.point_id = '$point_id' ";
      }

      $sqls   = "SELECT m.*, p.point_number
                 FROM t_menu m ,  t_point_service p
                 where m.is_active not in ('D') and p.point_id = m.point_id and m.agency_code = '$agencyCode' $con
                 ORDER BY p.point_number , m.start_time";

      //echo $sqls;
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      if($dataCount > 0)
      {
        foreach ($rows as $key => $value) {
          $is_active        = $value['is_active'];
          $menu_name        = $value['menu_name'];
          $menu_code        = $value['menu_code'];
          $type_menu        = $value['type_menu'];
          // $mem_username     = $value['mem_username']?;

          if($type_menu == "2"){
            $menu_code = "#";
            $menu_name = "พัก";
          }

          $activeTxt  = "";
          $bg         = "";
          $onclick    = "";
          $disabled   = "";

          $activeTxtR  = "";
          $bgR         = "";



          if($is_active == "Y")
          {
            $activeTxt = "ใช้งาน";
            $bg        = "bg-green-active";
          }else if($is_active == "N"){
            $activeTxt = "ไม่ใช้งาน";
            $bg        = "bg-gray";
          }

          // if($reserve_status == "Y")
          // {
          //   $activeTxtR = "เปิดจอง";
          //   $bgR        = "bg-green-active";
          // }else if($reserve_status == "N"){
          //   $activeTxtR = "ไม่เปิดจอง";
          //   $bgR        = "bg-gray";
          // }

    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="center"><?=$value['point_number']?></td>
      <td align="center"><?=$menu_code?></td>
      <td align="left"><?=$menu_name ?></td>
      <td align="center"><?=$value['start_time']." - ".$value['end_time']?></td>
      <td align="center"><?=$value['num_reserve']?></td>
      <td><div class="<?=$bg?>"><?=$activeTxt ?></div></td>
      <td>
        <a class="btn_point text-green"><i class="fa fa-eye" onclick="showForm('VIEW','<?=$value['menu_id']?>')"></i></a>
      </td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['menu_id']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="del('<?=$value['menu_id']?>','<?=$value['menu_name']?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php }} ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });
  })
</script>
