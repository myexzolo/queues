<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$agencyCode = $_SESSION['AGENCY_CODE'];

$menu_name          = "";
$menu_name_en       = "";
$service_id_list    = "";
$point_id           = "";
$menu_code          = "";
$start_time         = "";
$end_time           = "";
$num_reserve        = "";
$type_menu          = "1";
$type_service       = "2";
$is_active          = "";


$disabled   = "";
$disabled2  = "disabled";
$bg         = "background-color:#fff;";
$display    = "";

$ldt = "checked";
$lde = "";

if($action == 'ADD')
{
  $disabled2  = "";
}


if($action == 'EDIT' || $action == 'VIEW')
{
  if($action == 'VIEW')
  {
    $disabled = "disabled";
    $bg       = "";
    $display    = "display:none;";
  }

  $sql   = "SELECT * FROM t_menu WHERE menu_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;
  $menu_name          = $row[0]['menu_name'];
  $menu_name_en       = $row[0]['menu_name_en'];
  $menu_code          = $row[0]['menu_code'];
  $start_time         = $row[0]['start_time'];
  $end_time           = $row[0]['end_time'];
  $type_menu          = $row[0]['type_menu'];
  $type_service       = $row[0]['type_service'];
  $service_id_list    = !empty(@$row[0]['service_id_list'])?$row[0]['service_id_list']:"";
  $point_id           = $row[0]['point_id'];
  $is_active          = $row[0]['is_active'];
  $num_reserve        = $row[0]['num_reserve'];

}
  $optionPoint = getoptionPoint($point_id,$agencyCode);
?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="service_id_list" id="service_id_list" value="<?=$service_id_list?>">
<input type="hidden" name="menu_id" value="<?=$id?>">
<input type="hidden" name="agency_code" value="<?=$agencyCode?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-2">
      <div class="form-group">
        <label>ประเภทบริการ</label>
        <div class="radio">
          <label>
            <input type="radio" name="type_menu" value="1" <?=@$type_menu=='1'?"checked":""?>  onchange="chkService('1')"> เมนู&nbsp;&nbsp;
          </label>
          <label>
            <input type="radio" name="type_menu" value="2" <?=@$type_menu=='2'?"checked":""?> onchange="chkService('2')"> พัก
          </label>
        </div>
      </div>
    </div>
    <div class="col-md-3" style="display:none">
      <div class="form-group">
        <label>ประเภทเมนู</label>
        <div class="radio">
          <label>
            <input type="radio" name="type_service" value="2" <?=@$type_service=='2'?"checked":""?>> เมนูย่อย&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          </label>
          <label>
            <input type="radio" name="type_service" value="1" <?=@$type_service=='1'?"checked":""?>> เมนูหลัก
          </label>
        </div>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>จุดบริการที่</label>
        <select name="point_id" <?= $disabled ?> id="point_id" class="form-control select2" style="width: 100%;" required onchange="listService()">
          <option value="">&nbsp;</option>
          <?= $optionPoint ?>
        </select>
      </div>
    </div>
      <div class="col-md-2">
        <div class="form-group">
          <label>เวลาเปิด</label>
          <input type="text" <?= $disabled ?> value="<?= $start_time ?>" name="start_time" class="form-control"  placeholder="00:00" required maxlength="5">
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label>เวลาปิด</label>
          <input type="text" <?= $disabled ?> value="<?= $end_time ?>" name="end_time" class="form-control"  placeholder="00:00" required maxlength="5">
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label>รหัสบริการ</label>
          <input type="text" <?= $disabled ?> value="<?= $menu_code ?>" name="menu_code" class="form-control service"  placeholder="" required maxlength="1">
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label>สถานะ</label>
          <select name="is_active" <?= $disabled ?> class="form-control " style="width: 100%;" required >
            <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
            <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
          </select>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>จำนวนรับจองคิวต่อวัน</label>
          <input type="text" <?= $disabled ?> OnKeyPress="return chkNumber(this)" value="<?= @$num_reserve ?>" name="num_reserve" class="form-control">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>ชื่อเมนู</label>
          <input type="text" <?= $disabled ?> value="<?= $menu_name ?>"  name="menu_name" class="form-control service"  placeholder="" required>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>ชื่อเมนู (ภาษาอังกฤษ)</label>
          <input type="text" <?= $disabled ?> value="<?= $menu_name_en ?>"  name="menu_name_en" class="form-control service"  placeholder="">
        </div>
      </div>


    </div>
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">งานบริการ</h3>
        </div>
        <div class="box-body">
          <div id="listService"></div>
        </div>

      <!-- /.box-body -->
    </div>
        <!-- /.row -->
    <!-- </div> -->
  <!-- </div> -->
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
    chkService('<?=$type_menu?>');
    listService();
  })
</script>
