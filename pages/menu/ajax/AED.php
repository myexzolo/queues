<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action               = isset($_POST['action'])?$_POST['action']:"";
$menu_id              = isset($_POST['menu_id'])?$_POST['menu_id']:"";
$service_id_list      = isset($_POST['service_id_list'])?$_POST['service_id_list']:"";

// $user_id  = $_SESSION['member']['']

//print_r($_POST);
unset($_POST["action"]);

if($service_id_list != "")
{
  $_POST["service_id_list"] = implode(",",$service_id_list);
}
$_POST["status_send"] = "N"; //เพิ่ม

if($action == "ADD")
{
    unset($_POST["menu_id"]);
    $_POST["date_create"] = date('Y/m/d H:i:s');
    $sql = DBInsertPOST($_POST,'t_menu');
}
else if($action == "EDIT")
{
    $sql = DBUpdatePOST($_POST,'t_menu','menu_id');
}
else if($action == "DEL")
{
    $sql = "UPDATE t_menu SET is_active = 'D', status_send = 'N' WHERE menu_id = '$menu_id'";

}

// echo $sql;

$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($row['errorInfo'][0]) == 0){

    if($action == "ADD")
    {
      $menu_id  = $row['id']; ///แก้ ID
    }

    $sqls   = "SELECT m.*, p.ref_code as point_ref_code
               FROM t_menu m, t_point_service p
               where m.menu_id = '$menu_id' and m.point_id = p.point_id "; //เปลี่ยน table

    $querys     = DbQuery($sqls,null);
    $json       = json_decode($querys, true);
    $rows       = $json['data'][0];

    $rows["action"]   = "U";
    $agency_code = $rows["agency_code"];

    if($action == 'ADD')
    {
      $rows["ref_code"] = $agency_code."#C".$menu_id;   ///แก้ ID
      $rows["action"]   = "ADD";
    }

    $rows  = chkDataEmpty($rows);


    $ipAgency    = getIPbyAgency($agency_code);

    $data_array  = array(
                       "functionName" => "manageMenu",  ///แก้ ชื่อ Service
                       "dataJson" => $rows,
                     );

    $url        = "http://$ipAgency/ws/service.php";

    // echo $url;
    //echo json_encode($data_array);

    $make_call = callAPI('POST', $url, json_encode($data_array));
    $response = json_decode($make_call, true);
    $status   = $response['status'];
    $data     = $response['data'];

    $arrUpdate['menu_id']               =  $menu_id; //แก้ ID
    $arrUpdate['ref_code']              =  $rows["ref_code"];
    $arrUpdate['status_send']           =  "N";

    //print_r($response);
    $sql = "";

    if($status == "200")
    {
        $arrUpdate['status_send']  =  "S";
    }else{
      $arrLog['url']  = $url;
      $arrLog['data'] = json_encode($data_array,JSON_UNESCAPED_UNICODE);
      $arrLog['table_name']   = 't_menu';   // เปลี่ยน Table
      $arrLog['id_update']    = 'menu_id';// เปลี่ยน ID
      $arrLog['date_create']  = date('Y/m/d H:i:s');
      $arrLog['data_update']  = json_encode($arrUpdate);

      $sql = DBInsertPOST($arrLog,'t_log_send_service'); ///แก้ ชื่อ table
    }

    $sql .= DBUpdatePOST($arrUpdate,'t_menu','menu_id'); ///แก้ ชื่อ table
    DbQuery($sql,null);

    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}



?>
