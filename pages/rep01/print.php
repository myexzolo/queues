  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';



  $startDate  = $_POST['startDate'];
  $endDate    = $_POST['endDate'];

  $startDateStr = DatetoThaiMonth($startDate);
  $endDateStr   = DatetoThaiMonth($endDate);

  $textDate = "";
  if($startDate == $endDate){
    $textDate = "วันที่ ".$startDateStr;
  }else{
    $textDate = "ตั้งแต่วันที่ ".$startDateStr." - ".$endDateStr;
  }

  $startDate  = $_POST['startDate']." 00:00:01";
  $endDate    = $_POST['endDate']." 23:59:59";

  $sql = "SELECT q.agency_code, a.agency_name,
            SUM(CASE WHEN q.type_queue = 1 THEN 1 ELSE 0 END) AS walkin,
            SUM(CASE WHEN q.type_queue = 2 THEN 1 ELSE 0 END) AS reserve,
            SUM(CASE WHEN q.type_queue = 2 THEN 1 ELSE 0 END) AS reserve,
            SUM(CASE WHEN q.status_queue = 'C' THEN 1 ELSE 0 END) AS cancel,
            count(trans_queue_id ) AS total
          FROM t_trans_queue q, t_agency a
          WHERE q.date_start between '$startDate' and '$endDate' and  q.agency_code = a.agency_code
          GROUP BY q.agency_code
          ORDER BY count(trans_queue_id) DESC
          limit 0,10";
  $querys     = DbQuery($sql,null);
  $json       = json_decode($querys, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $rows       = $json['data'];

  // echo $imagePath;

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'mode' => 'utf-8',
      'format' => 'A4',
      'fontDir' => array_merge($fontDirs, [
           '../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'kanits' => [
              'R' => 'CSChatThai.ttf'
          ]
      ],
      'default_font' => 'kanits'
  ]);
  ob_start();
  // print_r($result);
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <link rel="stylesheet" href="css/pdf.css">
  </head>
  <body>
    <htmlpageheader name="firstpageheader">
      <div class="header" >
         <table width = "100%">
          <tr class="text-center">
              <td style="text-align:left;width:50px;"><img style="height:40px" src="../../image/logo.png"/></td>
              <td style="text-align:left;"><p>ระบบสนับสุนนการให้บริการ (Queue)</p></td>
              <td style="text-align:right;width:30%"><p>page {PAGENO} of {nb}</p></td>
           </tr>
         </table>
        </div>
    </htmlpageheader>
    <sethtmlpageheader name="firstpageheader" value="on" show-this-page="1" />
      <br>
      <div align="center" style="font-size:24px;font-weight:600">รายงานสรุปสาขาที่มีผู้เข้ารับบริการมากที่สุด 10 สาขา</div>
      <div align="center" style="font-size:22px;font-weight:600"><?=$textDate?></div><br>
    <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; overflow: wrap;" width = "100%">
      <thead>
        <tr class="text-center">
          <th class="thStyle" style="width:60px;">ลำดับที่</th>
          <th class="thStyle" >สำนักงานประกันสังคม สาขา</th>
          <th class="thStyle" style="width:130px;">ผู้มาติดต่อทั้งหมด<br>จำนวน (ราย)</th>
          <th class="thStyle" style="width:130px;">กดคิวที่ตู้ Kiosk<br>จำนวน (ราย)</th>
          <th class="thStyle" style="width:130px;">จองคิวล่วงหน้า<br>จำนวน (ราย)</th>
          <th class="thStyle" style="width:130px;">พลาดการรับบริการ<br>จำนวน (ราย)</th>
        </tr>
      </thead>
      <tbody>
      <?php
          for($i = 0; $i < $dataCount; $i++)
          {

          ?>
            <tr class="text-center">
              <td class="content"><?=$i+1;?></td>
              <td class="content" style="text-align:left;"><?= $rows[$i]['agency_name'] ?></td>
              <td class="content" style="text-align:right;"><?= $rows[$i]['total'] ?></td>
              <td class="content" style="text-align:right;"><?= $rows[$i]['walkin'] ?></td>
              <td class="content" style="text-align:right;"><?= $rows[$i]['reserve'] ?></td>
              <td class="content" style="text-align:right;"><?= $rows[$i]['cancel'] ?></td>
            </tr>
          <?php
          }
        ?>
      </tbody>
    </table>
  </body>
</html>
<?php
  $footer = '<div class="foot">
              <table width = "100%">
                <tr class="text-center">
                    <td style="text-align:left;width:50%"><p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'</td>

                 </tr>
               </table>
              </div>';

   $header  = '<div class="header" >
                <table width = "100%">
                 <tr class="text-center">
                     <td style="text-align:left;width:50px;"><img style="height:40px" src="../../image/logo.png"/></td>
                     <td style="text-align:left;"><p>ระบบสนับสุนนการให้บริการ (Queue)</p></td>
                     <td style="text-align:right;width:30%"><p>page {PAGENO} of {nb}</p></td>
                  </tr>
                </table>
               </div>';

    //echo $html;
    $html = ob_get_contents();
    ob_end_clean();
    ob_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('รายงานสรุปสาขาที่มีผู้เข้ารับบริการมากที่สุด 10 สาขา');
    // $mpdf->StartProgressBarOutput(2);
    // $mpdf->AddPage('P','','','','',left,right,top,10,10,10);
    // $mpdf->AddPage('P','','','','',10,10,12,10,10,10);
    $mpdf->autoPageBreak = true;
    $mpdf->AddPage('L','','','','',10,10,20,15,5,5);
    $mpdf->SetHTMLHeader($header);
    $mpdf->SetHTMLFooter($footer);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('rep01.pdf', \Mpdf\Output\Destination::INLINE);
    // $mpdf->Output('rep01.pdf', \Mpdf\Output\Destination::FILE);
  ?>
