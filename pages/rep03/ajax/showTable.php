<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<style>
th{
  vertical-align: middle !important;
}
</style>
<?php
$startDate  = $_POST['startDate'];
$endDate    = $_POST['endDate'];
$agencyStr  = $_POST['agencyStr'];

$startDateStr = DatetoThaiMonth($startDate);
$endDateStr   = DatetoThaiMonth($endDate);

$textDate = "";
if($startDate == $endDate){
  $textDate = "วันที่ ".$startDateStr;
}else{
  $textDate = "ตั้งแต่วันที่ ".$startDateStr." - ".$endDateStr;
}

$startDate  = $_POST['startDate']." 00:00:01";
$endDate    = $_POST['endDate']." 23:59:59";

$strAgency = "ทุกสาขา";
$con = "";
if($agencyStr != ""){
  $arrAgency = explode(":", $agencyStr);
  $con = " and q.agency_code = '$arrAgency[0]'";
  $strAgency = $arrAgency[1];
}

$sql = "SELECT q.*,TIMESTAMPDIFF(SECOND,q.date_start,q.date_end) as time_service, a.agency_name, s.kpi_time_a, s.service_name_a
        FROM t_trans_queue q, t_agency a, t_service_agency s
        WHERE q.date_start between '$startDate' and '$endDate' and status_queue = 'E'
        and  q.agency_code = a.agency_code and q.agency_code = s.agency_code and q.service_id = s.service_id $con
        ORDER BY q.agency_code,q.date_start";

$querys     = DbQuery($sql,null);
$json       = json_decode($querys, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$rows       = $json['data'];

?>
<div class="box-body">
<div align="center">
  <p style="font-size:24px;font-weight:600">รายงานสรุปเวลาที่ใช้ในการให้บริการต่อรายการ</p>
  <p style="font-size:24px;font-weight:600"><?= $strAgency ?></p>
  <p style="font-size:22px;font-weight:600"><?=$textDate?></p>
</div>
<input type="hidden" id="dataCount" value="<?=$dataCount?>">
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px;">ลำดับที่</th>
      <?php
      if($agencyStr == "")
      {
        echo "<th>สำนักงานประกันสังคม</th>";
      }
      ?>
      <th style="width:60px;">รหัสคิว</th>
      <th >งานบริการ</th>
      <th style="width:180px;">เวลาเข้ารับบริการ</th>
      <th style="width:180px;">เวลาสิ้นสุดการให้บริการ</th>
      <th style="width:150px;">เวลาที่ให้บริการ (นาที)</th>
    </tr>
  </thead>
<tbody>
  <?php
      $numShow = 0;
      $agencyCodeTmp = "";
      for($i = 0; $i < $dataCount; $i++)
      {
          $kpi_time_a   = $rows[$i]['kpi_time_a'];
          $time_service = $rows[$i]['time_service'];
          $agency_code  = $rows[$i]['agency_code'];
          $time_service = gmdate("H:i:s", $time_service);

          if($agencyCodeTmp != $agency_code)
          {
            $numShow = 0;
            $agencyCodeTmp = $agency_code;
          }

          $agency_name   = $rows[$i]['agency_name'];
          $numShow++;

      ?>
        <tr class="text-center">
          <td ><?=$numShow;?><input type="hidden" id="agencyStr" value="<?= $agencyStr ?>"></td>
          <?php
          if($agencyStr == "")
          {
            echo "<td style=\"text-align:left;\">$agency_name</td>";
          }
          ?>
          <td style="text-align:center;"><?= $rows[$i]['queue_code'] ?></td>
          <td style="text-align:left;"><?= $rows[$i]['service_name_a'] ?></td>
          <td style="text-align:center;"><?= DateThaiFull($rows[$i]['date_start']) ?></td>
          <td style="text-align:center;"><?= DateThaiFull($rows[$i]['date_end']) ?></td>
          <td style="text-align:center;"><?= $time_service ?></td>
        </tr>
      <?php
      }
    ?>
  </tbody>
</table>
</div>
<div class="box-footer">
  <button type="button" class="btn btn-success btn-flat pull-right btn-rep" onclick="printPdf()" >พิมพ์</button>
</div>
<script>
  $(function () {
    var groupColumn = 1;
    var agencyStr = $('#agencyStr').val();

    if(agencyStr != "")
    {
      $('#tableDisplay').DataTable({
        'paging'        : true,
        'lengthChange'  : false,
        'searching'     : false,
        'ordering'      : false,
        'info'          : true,
        'autoWidth'     : false
      })
    }else
    {
      $('#tableDisplay').DataTable({
        'paging'        : true,
        'lengthChange'  : false,
        'searching'     : false,
        'ordering'      : false,
        'info'          : true,
        'autoWidth'     : false,
        'columnDefs'    : [{ "visible": false, "targets": groupColumn }],
        'order'         : [[ groupColumn, 'asc' ]],
        'drawCallback'  : function ( settings ) {
                          var api = this.api();
                          var rows = api.rows( {page:'current'} ).nodes();
                          var last=null;

                          api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                            if ( last !== group )
                            {
                              $(rows).eq( i ).before(
                                  '<tr class="group"><td colspan="8"><b>'+group+'</b></td></tr>'
                              );
                              last = group;
                            }
                        } );
                        }
      })
    }

  })
</script>
