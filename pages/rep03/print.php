  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';



  $startDate  = $_POST['startDate'];
  $endDate    = $_POST['endDate'];
  $agencyStr  = $_POST['agencyStr'];

  $startDateStr = DatetoThaiMonth($startDate);
  $endDateStr   = DatetoThaiMonth($endDate);

  $textDate = "";
  if($startDate == $endDate){
    $textDate = "วันที่ ".$startDateStr;
  }else{
    $textDate = "ตั้งแต่วันที่ ".$startDateStr." - ".$endDateStr;
  }

  $startDate  = $_POST['startDate']." 00:00:01";
  $endDate    = $_POST['endDate']." 23:59:59";

  $strAgency = "ทุกสาขา";
  $con = "";
  if($agencyStr != ""){
    $arrAgency = explode(":", $agencyStr);
    $con = " and q.agency_code = '$arrAgency[0]'";
    $strAgency = $arrAgency[1];
  }

  $sql = "SELECT q.*,TIMESTAMPDIFF(SECOND,q.date_start,q.date_end) as time_service, a.agency_name, s.kpi_time_a, s.service_name_a
          FROM t_trans_queue q, t_agency a, t_service_agency s
          WHERE q.date_start between '$startDate' and '$endDate' and status_queue = 'E'
          and  q.agency_code = a.agency_code and q.agency_code = s.agency_code and q.service_id = s.service_id $con
          ORDER BY q.agency_code,q.date_start";


  $querys     = DbQuery($sql,null);
  $json       = json_decode($querys, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $rows       = $json['data'];

  // echo $imagePath;

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'mode' => 'utf-8',
      'format' => 'A4',
      'fontDir' => array_merge($fontDirs, [
           '../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'kanits' => [
              'R' => 'CSChatThai.ttf'
          ]
      ],
      'default_font' => 'kanits'
  ]);
  ob_start();
  // print_r($result);
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <link rel="stylesheet" href="css/pdf.css">
  </head>
  <body>
    <htmlpageheader name="firstpageheader">
      <div class="header" >
         <table width = "100%">
          <tr class="text-center">
              <td style="text-align:left;width:50px;"><img style="height:40px" src="../../image/logo.png"/></td>
              <td style="text-align:left;"><p>ระบบสนับสุนนการให้บริการ (Queue)</p></td>
              <td style="text-align:right;width:30%"><p>page {PAGENO} of {nb}</p></td>
           </tr>
         </table>
        </div>
    </htmlpageheader>
    <sethtmlpageheader name="firstpageheader" value="on" show-this-page="1" />
      <br>
      <div align="center" style="font-size:24px;font-weight:600">รายงานสรุปเวลาที่ใช้ในการให้บริการต่อรายการ</div>
      <div align="center" style="font-size:24px;font-weight:600"><?= $strAgency ?></div>
      <div align="center" style="font-size:22px;font-weight:600"><?=$textDate?></div><br>

    <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; overflow: wrap;" width = "100%">
    <thead>
      <tr class="text-center">
        <th class="thStyle" style="width:60px;">ลำดับที่</th>
        <th class="thStyle" style="width:60px;">รหัสคิว</th>
        <th class="thStyle" >งานบริการ</th>
        <th class="thStyle" style="width:180px;">เวลาเข้ารับบริการ</th>
        <th class="thStyle" style="width:180px;">เวลาสิ้นสุดการให้บริการ</th>
        <th class="thStyle" style="width:150px;">เวลาที่ให้บริการ (นาที)</th>
      </tr>
    </thead>
    <tbody>
      <?php
          $numShow = 0;
          $agencyCodeTmp = "";
          for($i = 0; $i < $dataCount; $i++)
          {
            $agency_code  = $rows[$i]['agency_code'];
            $agency_name  = $rows[$i]['agency_name'];

            $flag = "";
            if($agencyStr == "")
            {
              if($agencyCodeTmp != $agency_code)
              {
                $numShow = 0;
                $flag = "Y";

                if($agencyCodeTmp != "" && $i > 0)
                {
                  echo "
                      </tbody>
                    </table>
                    <div class=\"break\"></div>
                    <table border=\"1\" cellspacing=\"0\" style=\"border-collapse:collapse; border:solid #333 1px; overflow: wrap;\" width = \"100%\">
                    <thead>
                      <tr class=\"text-center\">
                        <th class=\"thStyle\" style=\"width:60px;\">ลำดับที่</th>
                        <th class=\"thStyle\" style=\"width:60px;\">รหัสคิว</th>
                        <th class=\"thStyle\" >งานบริการ</th>
                        <th class=\"thStyle\" style=\"width:180px;\">เวลาเข้ารับบริการ</th>
                        <th class=\"thStyle\" style=\"width:180px;\">เวลาสิ้นสุดการให้บริการ</th>
                        <th class=\"thStyle\" style=\"width:150px;\">เวลาที่ให้บริการ (นาที)</th>
                      </tr>
                    </thead>
                    <tbody>
                  ";
                }
                $agencyCodeTmp = $agency_code;
              }
            }

            $kpi_time_a   = $rows[$i]['kpi_time_a'];
            $time_service = $rows[$i]['time_service'];
            $time_service = gmdate("H:i:s", $time_service);
            if($kpi_time_a == 0)
            {
              $kpi_time_a = "-";
            }else
            {
              $kpi_time_a = $kpi_time_a;
            }

            // $agency_name   = str_replace("สำนักงานประกันสังคม","",$rows[$i]['agency_name']);
            $numShow++;

            if($flag == 'Y')
            {
              echo "
              <tr class=\"text-center\">
                <td class=\"content\" colspan=\"6\" style=\"text-align:left;\">$agency_name</td>
              </tr>
              ";
            }
            ?>
            <tr class="text-center">
              <td class="content" style="text-align:center;"><?=$numShow;?></td>
              <td class="content" style="text-align:center;"><?= $rows[$i]['queue_code'] ?></td>
              <td class="content" style="text-align:left;"><?= $rows[$i]['service_name_a'] ?></td>
              <td class="content" style="text-align:center;"><?= DateThaiFull($rows[$i]['date_start']) ?></td>
              <td class="content" style="text-align:center;"><?= DateThaiFull($rows[$i]['date_end']) ?></td>
              <td class="content" style="text-align:center;"><?= $time_service ?></td>
            </tr>
          <?php
          }
        ?>
      </tbody>
    </table>
  </body>
</html>
<?php
  $footer = '<div class="foot">
              <table width = "100%">
                <tr class="text-center">
                    <td style="text-align:left;width:50%"><p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'</td>

                 </tr>
               </table>
              </div>';

   $header  = '<div class="header" >
                <table width = "100%">
                 <tr class="text-center">
                     <td style="text-align:left;width:50px;"><img style="height:40px" src="../../image/logo.png"/></td>
                     <td style="text-align:left;"><p>ระบบสนับสุนนการให้บริการ (Queue)</p></td>
                     <td style="text-align:right;width:30%"><p>page {PAGENO} of {nb}</p></td>
                  </tr>
                </table>
               </div>';


    $html = ob_get_contents();
    ob_end_clean();
    ob_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('รายงานสรุปเวลาที่ใช้ในการให้บริการต่อรายการ');
    // $mpdf->StartProgressBarOutput(2);
    // $mpdf->AddPage('P','','','','',left,right,top,10,10,10);
    // $mpdf->AddPage('P','','','','',10,10,12,10,10,10);
    $mpdf->AddPage('L','','','','',10,10,20,15,5,5);
    $mpdf->SetHTMLHeader($header);
    $mpdf->SetHTMLFooter($footer);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('rep03.pdf', \Mpdf\Output\Destination::INLINE);
    // $mpdf->Output('rep01.pdf', \Mpdf\Output\Destination::FILE);
  ?>
