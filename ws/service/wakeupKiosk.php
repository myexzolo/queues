<?php
    include('../inc/function/mainFunc.php');
    include('../inc/function/connect.php');
    require_once '../PHPGangsta/WakeOnLAN.php';

    header("Content-type:text/html; charset=UTF-8");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);


    $status  = 404;
    $message = '404 Not Found';
    $status  = false;

    $dataJson     = isset($request['dataJson'])?$request['dataJson']:"";

    unset($request['type']);
    unset($request['functionName']);


    if($dataJson != "")
    {
      //print_r($dataJson);
      $ip             = $dataJson['ip'];
      $mac            = $dataJson['mac'];
      $ref_code       = $dataJson['ref_code'];
      $agency_code    = $dataJson['agency_code'];
      // $ip  = '192.168.0.247';
      // $mac = '48-BA-4E-3F-CD-B4';
      echo $ip.",".$mac;
      $mac  = str_replace("-",":",$mac);

      $status = \PHPGangsta\WakeOnLAN::wakeUp($mac, $ip);
    }

    //echo $status;
    if($status)
    {
      header('Content-Type: application/json');
      exit(json_encode(array('status' => $status,'message' => 'success')));
    }else
    {
      header('Content-Type: application/json');
      exit(json_encode(array('status' => $status,'message' => 'fail')));
    }




?>
