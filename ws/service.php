<?php
include('config.php');
$status = 404;
$message = 'functionName is not macth';
$data = array();

$postdata = file_get_contents("php://input");
$request = json_decode($postdata,true);
$functionName = @$request['functionName'];
switch (@$request['functionName']) {
  //ALL
  case 'getPerformanceInfo':
    include('service/getPerformanceInfo.php');
    break;
  case 'getAgency':
    include('service/getAgency.php');
    break;
  case 'getUser':
    include('service/getUser.php');
    break;
  case 'getService':
    include('service/getService.php');
    break;
  case 'getRole':
    include('service/getRole.php');
    break;
  case 'setUser':
    include('service/setUser.php');
    break;
  case 'setStatusSend':
    include('service/setStatusSend.php');
    break;
  case 'managePointService':
    include('service/managePointService.php');
    break;
  case 'manageServiceChanel':
    include('service/manageServiceChanel.php');
    break;
  case 'manageServiceAgency':
    include('service/manageServiceAgency.php');
    break;
  case 'manageReserve':
    include('service/manageReserve.php');
    break;
  case 'manageTransQueue':
    include('service/manageTransQueue.php');
    break;
  case 'manageMenu':
    include('service/manageMenu.php');
    break;
  case 'manageKiosk':
    include('service/manageKiosk.php');
    break;
  case 'setLastActive':
    include('service/setLastActive.php');
    break;
  case 'wakeupKiosk':
    include('service/wakeupKiosk.php');
    break;
  default:
    break;
}
exit(json_encode(
  array( "status" => $status , "message" => $message ,
  "data" => $data ,"functionName" => $functionName  ) ));

?>
