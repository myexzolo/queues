$('#formRegister').on('submit', function(event) {
  event.preventDefault();
  if ($('#formRegister').smkValidate()) {
    // Code here
    if( $.smkEqualPass('#pass', '#confirmPass') ){
      $.ajax({
          url: 'service/AEDregister.php',
          type: 'POST',
          data: new FormData( this ),
          processData: false,
          contentType: false,
          dataType: 'json'
      }).done(function( data ) {
        $.smkAlert({text: data.message,type:data.status,position:'bottom-right'});
        if(data.status == 'success'){
          window.location = '../home/';
        }
      });
    }else{
      $.smkAlert({text:'รหัสผ่านไม่ตรงกัน',type:'danger',position:'bottom-right'});
    }
  }
});



function checkIDCard(){
  var idCard = $("#idcard").val();
  var idCards = parseInt($("#idcard").val());
  if(idCard.length == 13){
    $.post( "service/checkIdcard.php", { idCard: idCard })
    .done(function( data ) {
      if(data.status == 'danger'){
        $.smkAlert({text: data.message,type:data.status,position:'bottom-right'});
        $("#idcard").val('');
      }
    });
  }else{
    if(!Number.isInteger(idCards)){
      $.smkAlert({text:'หมายเลขบัตรประชาชนไม่ถูกต้อง',type:'danger',position:'bottom-right'});
    }else{
      $.smkAlert({text:'หมายเลขบัตรประชาชนไม่ถูกต้อง',type:'danger',position:'bottom-right'});
    }

  }
}
