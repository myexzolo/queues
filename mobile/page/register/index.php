<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../image/logo.png">
    <title>Home : สำนักงานประกันสังคม</title>

    <?php include "../../inc/css.php"; ?>
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body class="main-bg">
    <?php include "../../inc/nav.php"; ?>
    <?php $back='../login';$backname='กลับหน้าเข้าสู่ระบบ'; ?>
    <?php include "../../inc/back.php"; ?>
    <div class="box-form">

      <div class="row">
        <div class="col-xs-12">
          <img class="image-logo" src="../../image/logo.png">
        </div>
      </div>

      <!-- <form method="post" action="service/AEDregister.php"> -->
      <form id="formRegister" novalidate>
        <div class="row">
          <div class="col-xs-12">
            <div class="form-group">
              <input type="text" id="idcard" onfocusout="checkIDCard()" data-smk-pattern="[0-9]{13}" minlength="13" maxlength="13" name="mem_idcard" class="form-control" placeholder="เลขบัตรประชาชน" data-smk-msg="&nbsp;" required>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="form-group">
              <input type="text" name="mem_name" class="form-control" placeholder="ชื่อ" data-smk-msg="&nbsp;" required>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="form-group">
              <input type="text" name="mem_last" class="form-control" placeholder="นามสกุล" data-smk-msg="&nbsp;" required>
            </div>
          </div>

          <div class="col-xs-12">
            <label>วันเดือนปีเกิด</label>
          </div>
          <div class="col-xs-4">
            <div class="form-group">
              <select class="form-control" name="mem_day" required data-smk-msg="&nbsp;">
                <option value="">วันที่</option>
                <?php for ($i=1; $i <=31 ; $i++) { ?>
                <option value="<?=$i?>"><?=$i?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="col-xs-4 pdd-0">
            <div class="form-group">
              <select class="form-control" name="mem_month" required data-smk-msg="&nbsp;">
                <option value="">เดือน</option>
                <?php for ($i=1; $i <=12 ; $i++) { ?>
                <option value="<?=$i?>"><?=getMonth($i)?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="col-xs-4">
            <div class="form-group">
              <select class="form-control" name="mem_year" required data-smk-msg="&nbsp;">
                <option value="">ปี</option>
                <?php for ($i=date('Y')+543; $i >= date('Y')+543-100 ; $i--) { ?>
                <option value="<?=$i?>"><?=$i?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="form-group">
              <input type="password" id="pass" name="mem_pass" class="form-control" placeholder="รหัสผ่าน" data-smk-msg="&nbsp;" required>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="form-group">
              <input type="password" id="confirmPass" class="form-control" placeholder="รหัสผ่าน" data-smk-msg="&nbsp;" required>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="text-center">
              <button type="submit" class="btn btn-warning btn-sso">ลงทะเบียน</button>
            </div>
          </div>
        </div>
      </form>
    </div>



    <?php include "../../inc/footer.php"; ?>
    <?php include "../../inc/js.php"; ?>
    <script src="js/main.js"></script>
  </body>
</html>
